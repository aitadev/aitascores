package com.aitadev.aitascores.strategy;


import android.support.annotation.NonNull;

import com.aitadev.sdk.aitamodels.ListModel;
import com.aitadev.sdk.aitamodels.MatchFormat;
import com.aitadev.sdk.aitamodels.MatchStatModel;
import com.aitadev.sdk.aitamodels.PlayerScoreStatsModel;
import com.aitadev.sdk.aitamodels.SetModel;
import com.aitadev.sdk.aitamodels.StatsModel;

public class MatchStrategy {

    public static final int GAMES_TO_WIN_PER_SET = 6;
    private MatchFormat matchFormat;
    private String player;
    private StatsModel statsModel;
    private StatsModel submittedStatsModel;
    private boolean isTieBreakOn;
    private String serveStrategy;
    private String pointStrategy;
    private boolean isBreakPoint = false;

    public MatchStrategy(MatchFormat matchFormat, StatsModel statsModel) {
        this.matchFormat = matchFormat;
        this.isTieBreakOn = false;
        this.statsModel = statsModel;
    }

    public MatchFormat getMatchFormat() {
        return matchFormat;
    }

    public void setTieBreakOn(boolean tieBreakOn) {
        isTieBreakOn = tieBreakOn;
    }

    public void setMatchFormat(MatchFormat matchFormat) {
        this.matchFormat = matchFormat;
    }

    public String getServeStrategy() {
        return serveStrategy;
    }

    public void setServeStrategy(String serveStrategy) {
        this.serveStrategy = serveStrategy;
    }

    public String getPointStrategy() {
        return pointStrategy;
    }

    public boolean isTieBreakOn() {
        return isTieBreakOn;
    }

    public void setPointStrategy(String pointStrategy) {
        this.pointStrategy = pointStrategy;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public String getPlayer() {
        return this.player;
    }

    public StatsModel getStatsModel() {
        return statsModel;
    }

    public StatsModel getSubmittedStatsModel() {
        return submittedStatsModel;
    }


    public void setStatsModel(StatsModel statsModel) {
        this.statsModel = statsModel;
    }

    public void setSubmittedStatsModel(StatsModel statsModel) {
        this.submittedStatsModel = statsModel;
    }


    public String getServingPlayer() {
        return statsModel.getServingPlayer();
    }


    public void setMatchStatus(String status) {
        statsModel.setStatus(status);
    }

    public String getMatchStatus() {
        return statsModel.getStatus();
    }


    @NonNull
    private PlayerScoreStatsModel getPlayerScoreStatsModel(String player, String aitaId) {
        PlayerScoreStatsModel playerScoreStatsModel = new PlayerScoreStatsModel();
        playerScoreStatsModel.setAitaId(aitaId);
        playerScoreStatsModel.setName(player);
        SetModel setModel = new SetModel();
        setModel.setPointScore("0");
        setModel.setGameScore("0");
        ListModel<SetModel> setList = new ListModel<>();
        setList.items.add(setModel);
        playerScoreStatsModel.setSetList(setList);
        return playerScoreStatsModel;
    }

    public void calculateScore() {
        calculateNextPointScore();
    }

    public void calculateStats() {
        String servingPlayer = statsModel.getServingPlayer();
        String pointStrategy = getPointStrategy();
        String serveStrategy = getServeStrategy();
        String player = getPlayer();
        MatchStatModel matchStatModel1 = statsModel.getPlayer1ScoreStatsModel().getMatchStatModel();
        MatchStatModel matchStatModel2 = statsModel.getPlayer2ScoreStatsModel().getMatchStatModel();
        if ("player1".equals(player)) {
            if ("player1".equals(servingPlayer)) {
                serveStatsCalculation(serveStrategy, matchStatModel1, true);
                if (serveStrategy.equals("doublefault") == false)
                    pointsStatsCalculation(pointStrategy, matchStatModel1, matchStatModel2);
            } else if ("player2".equals(servingPlayer)) {
                serveStatsCalculation(serveStrategy, matchStatModel2, false);
                if (serveStrategy.equals("doublefault") == false)
                    pointsStatsCalculation(pointStrategy, matchStatModel1, matchStatModel2);
            }
        } else if ("player2".equals(player)) {
            if ("player2".equals(servingPlayer)) {
                serveStatsCalculation(serveStrategy, matchStatModel2, true);
                if (serveStrategy.equals("doublefault") == false)
                    pointsStatsCalculation(pointStrategy, matchStatModel2, matchStatModel1);
            } else if ("player1".equals(servingPlayer)) {
                serveStatsCalculation(serveStrategy, matchStatModel1, false);
                if (serveStrategy.equals("doublefault") == false)
                    pointsStatsCalculation(pointStrategy, matchStatModel2, matchStatModel1);
            }

        }
    }

    private void pointsStatsCalculation(String pointStrategy, MatchStatModel matchStatModel1, MatchStatModel matchStatModel2) {
        switch (pointStrategy) {
            case "ace":
                int aces = Integer.parseInt(matchStatModel1.getAces());
                matchStatModel1.setAces(String.valueOf(aces + 1));
                break;
            case "winner":
                int winners = Integer.parseInt(matchStatModel1.getWinners());
                matchStatModel1.setWinners(String.valueOf(winners + 1));
                break;
            case "error":
                int errors = Integer.parseInt(matchStatModel2.getErrors());
                matchStatModel2.setErrors(String.valueOf(errors + 1));
                break;
            case "doublefault":
            default:
                break;
        }
    }

    private void serveStatsCalculation(String serveStrategy, MatchStatModel matchStatModel1, boolean win) {
        if (serveStrategy == null)
            return;
        switch (serveStrategy) {
            case "first":
                firstServeStats(matchStatModel1, win);
                break;
            case "second":
                secondServeStats(matchStatModel1, win);
                break;
            case "doublefault":
                doubleFaultStats(matchStatModel1);
                break;
            default:
                break;
        }
    }

    private void doubleFaultStats(MatchStatModel matchStatModel2) {
        int doubleFaults = Integer.parseInt(matchStatModel2.getDoubleFaults());
        matchStatModel2.setDoubleFaults(String.valueOf(doubleFaults + 1));
    }

    private void secondServeStats(MatchStatModel matchStatModel1, boolean win) {
//        int totalFirstServe = 0;
        int totalSecondServe = 0;
//        if (matchStatModel1.getTotalFirstServe() != null)
//            totalFirstServe = Integer.parseInt(matchStatModel1.getTotalFirstServe());


        if (matchStatModel1.getTotalSecondServe() != null)
            totalSecondServe = Integer.parseInt(matchStatModel1.getTotalSecondServe());

        //matchStatModel1.setTotalFirstServe(String.valueOf(totalFirstServe + 1));
        matchStatModel1.setTotalSecondServe(String.valueOf(totalSecondServe + 1));
        if (win == true) {
            int secondServeWon = 0;
            if(matchStatModel1.getSecondServeWon() != null)
                secondServeWon = Integer.parseInt(matchStatModel1.getSecondServeWon());
            matchStatModel1.setSecondServeWon(String.valueOf(secondServeWon + 1));

        }
    }

    private void firstServeStats(MatchStatModel matchStatModel, boolean win) {
        int totalFirstServe = 0;
        if (matchStatModel.getTotalFirstServe() != null)
            totalFirstServe = Integer.parseInt(matchStatModel.getTotalFirstServe());

        int firstServeWon = Integer.parseInt(matchStatModel.getFirstServeWon());
        matchStatModel.setTotalFirstServe(String.valueOf(totalFirstServe + 1));
        if (win == true)
            matchStatModel.setFirstServeWon(String.valueOf(firstServeWon + 1));
    }

    private void calculateNextPointScore() {
        String liveScorep1 = getLivePoint(statsModel.getPlayer1ScoreStatsModel());
        String liveScorep2 = getLivePoint(statsModel.getPlayer2ScoreStatsModel());
        if (!isTieBreakOn) {
            calculateNextPoint(liveScorep1, liveScorep2);
        } else {
            calculateTieBreakScore(liveScorep1, liveScorep2);
        }
    }

    public void CalculatePostScoreStats() {
        String liveScorep1 = getLivePoint(statsModel.getPlayer1ScoreStatsModel());
        String liveScorep2 = getLivePoint(statsModel.getPlayer2ScoreStatsModel());
        MatchStatModel player1MatchStatModel = statsModel.getPlayer1ScoreStatsModel().getMatchStatModel();
        MatchStatModel player2MatchStatModel = statsModel.getPlayer2ScoreStatsModel().getMatchStatModel();
        setBreakPointFacedStat("player1", liveScorep1, liveScorep2, player2MatchStatModel);
        setBreakPointFacedStat("player2", liveScorep2, liveScorep1, player1MatchStatModel);
    }

    private void setBreakPointFacedStat(String player1orPlayer2, String servingPlayerLiveScore, String returnPlayerLiveScore, MatchStatModel returnPlayerStatModel) {
        String servingPlayer = getServingPlayer();
        if (player1orPlayer2.equalsIgnoreCase(servingPlayer)) {
            if ((returnPlayerLiveScore.equalsIgnoreCase("40") && !servingPlayerLiveScore.equalsIgnoreCase("40")) || (returnPlayerLiveScore.equalsIgnoreCase("A"))) {
                isBreakPoint = true;
                int breakPointFaced = 0;
                if (returnPlayerStatModel.getBreakPointsFaced() != null )
                    breakPointFaced = Integer.parseInt(returnPlayerStatModel.getBreakPointsFaced());
                returnPlayerStatModel.setBreakPointsFaced(String.valueOf(breakPointFaced + 1));
            } else {
                isBreakPoint = false;
            }
        }
    }

    private void calculateTieBreakScore(String liveScorep1, String liveScorep2) {
        ListModel<SetModel> player1SetData = statsModel.getPlayer1ScoreStatsModel().getSetList();
        ListModel<SetModel> player2SetData = statsModel.getPlayer2ScoreStatsModel().getSetList();
        int p1Score = Integer.parseInt(liveScorep1);
        int p2Score = Integer.parseInt(liveScorep2);
        int latestSet = player1SetData.items.size() - 1;
        int numPointsPerTieBreak = Integer.parseInt(matchFormat.getNumPointsPerTieBreak());

        if ("player1".equals(this.player)) {
            if (p1Score < numPointsPerTieBreak - 1) {
                p1Score++;
                player1SetData.items.get(latestSet).setPointScore(String.valueOf(p1Score));
            } else {
                if (p2Score <= p1Score - 1) {
                    p1Score++;
                    player1SetData.items.get(latestSet).setPointScore(String.valueOf(p1Score));
                    calculateNextGameScore();
                } else {
                    p1Score++;
                    player1SetData.items.get(latestSet).setPointScore(String.valueOf(p1Score));
                }
            }
        } else if ("player2".equals(this.player)) {
            if (p2Score < numPointsPerTieBreak - 1) {
                p2Score++;
                player2SetData.items.get(latestSet).setPointScore(String.valueOf(p2Score));
            } else {
                if (p1Score <= p2Score - 1) {
                    p2Score++;
                    player2SetData.items.get(latestSet).setPointScore(String.valueOf(p2Score));
                    calculateNextGameScore();
                } else {
                    p2Score++;
                    player2SetData.items.get(latestSet).setPointScore(String.valueOf(p2Score));
                }
            }
        }

        if ((p1Score + p2Score) % 2 == 1) {
            if (statsModel.getServingPlayer().equals("player1"))
                statsModel.setServingPlayer("player2");
            else
                statsModel.setServingPlayer("player1");
        }

    }

    private void calculateNextPoint(String liveScorep1, String liveScorep2) {
        ListModel<SetModel> player1SetData = statsModel.getPlayer1ScoreStatsModel().getSetList();
        ListModel<SetModel> player2SetData = statsModel.getPlayer2ScoreStatsModel().getSetList();
        int latestSet = player1SetData.items.size() - 1;

        if ("player1".equals(this.player)) {
            calculatePlayerPoint(liveScorep1, player1SetData, player2SetData, latestSet);
        } else if ("player2".equals(this.player)) {
            calculatePlayerPoint(liveScorep2, player2SetData, player1SetData, latestSet);

        }

    }

    private void calculatePlayerPoint(@NonNull String liveScore, ListModel<SetModel> winnerPlayerSetData, ListModel<SetModel> loserPlayerSetData, int latestSet) {
        switch (liveScore) {
            case "0":
                winnerPlayerSetData.items.get(latestSet).setPointScore("15");
                break;
            case "15":
                winnerPlayerSetData.items.get(latestSet).setPointScore("30");
                break;
            case "30":
                winnerPlayerSetData.items.get(latestSet).setPointScore("40");
                break;
            case "40":
                if ("true".equals(matchFormat.isSinglePointDeuce())) {
                    winnerPlayerSetData.items.get(latestSet).setPointScore("0");
                    loserPlayerSetData.items.get(latestSet).setPointScore("0");
                    calculateNextGameScore();
                } else {
                    if ((loserPlayerSetData.items.get(latestSet).getPointScore()).equals("40")) {
                        winnerPlayerSetData.items.get(latestSet).setPointScore("A");
                        loserPlayerSetData.items.get(latestSet).setPointScore("-");
                    } else {
                        winnerPlayerSetData.items.get(latestSet).setPointScore("0");
                        loserPlayerSetData.items.get(latestSet).setPointScore("0");
                        calculateBreakPointConvertedStat();
                        calculateNextGameScore();
                    }
                }
                break;
            case "-":
                winnerPlayerSetData.items.get(latestSet).setPointScore("40");
                loserPlayerSetData.items.get(latestSet).setPointScore("40");
                break;
            case "A":
                winnerPlayerSetData.items.get(latestSet).setPointScore("0");
                loserPlayerSetData.items.get(latestSet).setPointScore("0");
                calculateBreakPointConvertedStat();
                calculateNextGameScore();
                break;

        }
    }

    private void calculateBreakPointConvertedStat() {
        if(isBreakPoint) {
            if (this.player.equalsIgnoreCase("player1")) {
                MatchStatModel matchStatModel = statsModel.getPlayer1ScoreStatsModel().getMatchStatModel();
                setBreakPointConvertedStat(matchStatModel);
            } else {
                MatchStatModel matchStatModel = statsModel.getPlayer2ScoreStatsModel().getMatchStatModel();
                setBreakPointConvertedStat(matchStatModel);
            }
        }
    }

    private void setBreakPointConvertedStat(MatchStatModel matchStatModel) {
        int breakPointsConverted = 0;
        if (matchStatModel.getBreakPointsConverted() != null) {
            breakPointsConverted = Integer.parseInt(matchStatModel.getBreakPointsConverted());
        }
        matchStatModel.setBreakPointsConverted(String.valueOf(breakPointsConverted + 1));
    }

    private void calculateNextGameScore() {
        ListModel<SetModel> player1sets = statsModel.getPlayer1ScoreStatsModel().getSetList();
        ListModel<SetModel> player2sets = statsModel.getPlayer2ScoreStatsModel().getSetList();
        int latestSet = player1sets.items.size() - 1;
        if (statsModel.getServingPlayer().equals("player1"))
            statsModel.setServingPlayer("player2");
        else
            statsModel.setServingPlayer("player1");
        if ("player1".equals(this.player)) {
            int currentGameScoreP1 = Integer.parseInt(player1sets.items.get(latestSet).getGameScore().trim());
            int currentGameScoreP2 = Integer.parseInt(player2sets.items.get(latestSet).getGameScore().trim());

            if ("true".equals(matchFormat.getIsMatchSetFormat())) {
                if (currentGameScoreP1 < GAMES_TO_WIN_PER_SET - 1) {
                    currentGameScoreP1++;
                    player1sets.items.get(latestSet).setGameScore(String.valueOf(currentGameScoreP1));
                } else if (currentGameScoreP1 == GAMES_TO_WIN_PER_SET - 1) {
                    if (currentGameScoreP2 < currentGameScoreP1) {
                        calculateNextMatchScore();
                    } else if (currentGameScoreP2 == GAMES_TO_WIN_PER_SET) {
                        this.isTieBreakOn = true;
                    }
                    currentGameScoreP1++;
                    player1sets.items.get(latestSet).setGameScore(String.valueOf(currentGameScoreP1));
                } else if (currentGameScoreP1 == GAMES_TO_WIN_PER_SET) {
                    currentGameScoreP1++;
                    player1sets.items.get(latestSet).setGameScore(String.valueOf(currentGameScoreP1));
                    calculateNextMatchScore();
                }
            } else {
                int gamesToWinPerMatch = Integer.parseInt(matchFormat.getBestOfNgamesPerMatch()) / 2 + 1;
                if (currentGameScoreP1 < gamesToWinPerMatch - 1) {
                    currentGameScoreP1++;
                    player1sets.items.get(latestSet).setGameScore(String.valueOf(currentGameScoreP1));
                    if (currentGameScoreP1 == gamesToWinPerMatch - 1) {
                        if (currentGameScoreP2 == currentGameScoreP1) {
                            this.isTieBreakOn = true;
                        }
                    }
                } else if (currentGameScoreP1 == gamesToWinPerMatch - 1) {
                    if (currentGameScoreP2 < currentGameScoreP1) {
                        currentGameScoreP1++;
                        player1sets.items.get(latestSet).setGameScore(String.valueOf(currentGameScoreP1));
                        calculateNextMatchScore();
                    } else {
                        currentGameScoreP1++;
                        player1sets.items.get(latestSet).setGameScore(String.valueOf(currentGameScoreP1));// + "(" + player1sets.items.get(latestSet).getPointScore() + ")");
                        calculateNextMatchScore();
                    }
                } else {
                    currentGameScoreP1++;
                    player1sets.items.get(latestSet).setGameScore(String.valueOf(currentGameScoreP1));// + "(" + player1sets.items.get(latestSet).getPointScore() + ")");
                    calculateNextMatchScore();
                }
            }


        } else if ("player2".equals(this.player)) {
            int currentGameScoreP1 = Integer.parseInt(player1sets.items.get(latestSet).getGameScore().trim());
            int currentGameScoreP2 = Integer.parseInt(player2sets.items.get(latestSet).getGameScore().trim());

            if ("true".equals(matchFormat.getIsMatchSetFormat())) {
                int gamesToWinPerSet = 6;
                if (currentGameScoreP2 < gamesToWinPerSet - 1) {
                    currentGameScoreP2++;
                    player2sets.items.get(latestSet).setGameScore(String.valueOf(currentGameScoreP2));
                } else if (currentGameScoreP2 == gamesToWinPerSet - 1) {
                    if (currentGameScoreP1 < currentGameScoreP2) {
                        calculateNextMatchScore();
                    } else if (currentGameScoreP1 == gamesToWinPerSet) {
                        this.isTieBreakOn = true;
                    }
                    currentGameScoreP2++;
                    player2sets.items.get(latestSet).setGameScore(String.valueOf(currentGameScoreP2));
                } else if (currentGameScoreP2 == gamesToWinPerSet) {
                    currentGameScoreP2++;
                    player2sets.items.get(latestSet).setGameScore(String.valueOf(currentGameScoreP2));
                    calculateNextMatchScore();
                }
            } else {
                int gamesToWinPerMatch = Integer.parseInt(matchFormat.getBestOfNgamesPerMatch()) / 2 + 1;
                if (currentGameScoreP2 < gamesToWinPerMatch - 1) {
                    currentGameScoreP2++;
                    player2sets.items.get(latestSet).setGameScore(String.valueOf(currentGameScoreP2));
                    if (currentGameScoreP2 == gamesToWinPerMatch - 1) {
                        if (currentGameScoreP1 == currentGameScoreP2) {
                            this.isTieBreakOn = true;
                        }
                    }
                } else if (currentGameScoreP2 == gamesToWinPerMatch - 1) {
                    if (currentGameScoreP1 < currentGameScoreP2) {
                        currentGameScoreP2++;
                        player2sets.items.get(latestSet).setGameScore(String.valueOf(currentGameScoreP2));
                        calculateNextMatchScore();
                    } else {
                        currentGameScoreP2++;
                        player2sets.items.get(latestSet).setGameScore(String.valueOf(currentGameScoreP2));// + "(" + player2sets.items.get(latestSet).getPointScore() + ")");
                        calculateNextMatchScore();
                    }
                } else {
                    currentGameScoreP2++;
                    player2sets.items.get(latestSet).setGameScore(String.valueOf(currentGameScoreP2));// + "(" + player1sets.items.get(latestSet).getPointScore() + ")");
                    calculateNextMatchScore();
                }
            }


        }

    }

    private void calculateNextMatchScore() {
        ListModel<SetModel> player1sets = statsModel.getPlayer1ScoreStatsModel().getSetList();
        ListModel<SetModel> player2sets = statsModel.getPlayer2ScoreStatsModel().getSetList();
        int latestSet = player1sets.items.size() - 1;
        this.isTieBreakOn = false;
        if ("player1".equals(this.player)) {
            player1sets.items.get(latestSet).setResult("W");

            int matchScore = Integer.parseInt(statsModel.getPlayer1ScoreStatsModel().getMatchScore());
            matchScore++;
            statsModel.getPlayer1ScoreStatsModel().setMatchScore(String.valueOf(matchScore));

            int numWins = getWonSets(statsModel.getPlayer1ScoreStatsModel());
            if (numWins == Integer.parseInt(matchFormat.getBestOfNSetsPerMatch()) / 2 + 1) {
                statsModel.setStatus("Completed");
                statsModel.setWinner("player1");
            } else {
                addSetModel(player1sets);
                addSetModel(player2sets);
            }
        } else {
            player2sets.items.get(latestSet).setResult("W");

            int matchScore = Integer.parseInt(statsModel.getPlayer2ScoreStatsModel().getMatchScore());
            matchScore++;
            statsModel.getPlayer2ScoreStatsModel().setMatchScore(String.valueOf(matchScore));

            int numWins = getWonSets(statsModel.getPlayer2ScoreStatsModel());
            if (numWins == Integer.parseInt(matchFormat.getBestOfNSetsPerMatch()) / 2 + 1) {
                statsModel.setStatus("Completed");
                statsModel.setWinner("player2");

            } else {
                addSetModel(player1sets);
                addSetModel(player2sets);
            }

        }
    }

    private void addSetModel(ListModel<SetModel> playersets) {
        SetModel setModel = new SetModel();
        setModel.setPointScore("0");
        setModel.setGameScore("0");
        playersets.items.add(setModel);
    }

    private int getWonSets(PlayerScoreStatsModel player1) {
        int numSets = player1.getSetList().items.size();
        int numWins = 0;
        for (int i = 0; i < numSets; i++) {
            if ("W".equals(player1.getSetList().items.get(i).getResult())) {
                numWins++;
            }
        }
        return numWins;
    }


    private String getLivePoint(PlayerScoreStatsModel player) {
        int currentSet = player.getSetList().items.size() - 1;
        return player.getSetList().items.get(currentSet).getPointScore();
    }

}
