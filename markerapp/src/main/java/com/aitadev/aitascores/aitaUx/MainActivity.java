package com.aitadev.aitascores.aitaUx;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.aitadev.sdk.APINode;
import com.aitadev.sdk.ConnectionManager;
import com.aitadev.sdk.ConnectionTask;
import com.aitadev.sdk.aitamodels.StatsModel;
import com.aitascores.shsadago.aitascores.admin.R;

import org.json.JSONException;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private Map<String,String> patternValue = new HashMap<>();
    public static final String TID = "{tid}";
    public static final String MID = "{mid}";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_update);
        getStatsData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getStatsData();
    }


    private void getStatsData() {

        final EditText tid = findViewById(R.id.tid);
        final EditText mid = findViewById(R.id.mid);
        Button next = findViewById(R.id.next);

        final Context context = this;


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                patternValue.put(TID, tid.getText().toString());
                patternValue.put(MID, mid.getText().toString());
                ConnectionManager.getSharedInstance().doGet(ConnectionTask.obtainGetTask(APINode.ADMIN_LIVE_SCORE_STATS, context, null, patternValue), new ConnectionManager.ConnectionTaskListenerAdapter() {
                    @Override
                    public void onConnectionComplete(ConnectionTask task, String json) {
                        try {
                            super.onConnectionComplete(task, json);

                            Intent i = new Intent(context, AdminStatsActivity.class);

                            if(task.response.items!=null && task.response.items.get(0)!=null) {
                                i.putExtra("statsModel", (StatsModel)task.response.items.get(0));
                                context.startActivity(i);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InstantiationException e) {
                            e.printStackTrace();
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }

}
