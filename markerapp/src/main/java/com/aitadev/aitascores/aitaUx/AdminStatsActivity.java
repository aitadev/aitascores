package com.aitadev.aitascores.aitaUx;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.aitadev.aitascores.adapters.AdminLiveScoreAdapter;
import com.aitadev.aitascores.strategy.MatchStrategy;
import com.aitadev.sdk.APINode;
import com.aitadev.sdk.ConnectionManager;
import com.aitadev.sdk.ConnectionTask;
import com.aitadev.sdk.aitamodels.ListModel;
import com.aitadev.sdk.aitamodels.MatchFormat;
import com.aitadev.sdk.aitamodels.SetModel;
import com.aitadev.sdk.aitamodels.StatsModel;
import com.aitascores.shsadago.aitascores.admin.R;
import com.fasterxml.jackson.core.JsonProcessingException;

import org.apache.commons.lang3.SerializationUtils;
import org.json.JSONException;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdminStatsActivity extends AppCompatActivity {

    private Map<String,String> patternValue = new HashMap<>();
    private MatchFormat matchFormat;
    MatchStrategy matchStrategy;
    StatsModel statsModel;
    private RecyclerView rv;
    private Spinner winnerSelection;
    private Spinner servingPlayer;
    private Spinner matchStatus;
    private RadioGroup serveradio ;
    private RadioGroup player1stats;
    private RadioGroup player2stats;
    private Button undoButton;
    private Button submit;
    private TextView player1Name;
    private TextView player2Name;
    private TextView matchSchedule;
    public static final String TID = "{tid}";
    public static final String MID = "{mid}";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats_update);
        Intent i = getIntent();
        statsModel = (StatsModel) i.getSerializableExtra("statsModel");

        matchStrategy = new MatchStrategy(matchFormat, statsModel);
        matchStrategy.getStatsModel().setStatus("InProgress");
        matchStrategy.getStatsModel().setServingPlayer("player1");

        matchStrategy.setSubmittedStatsModel(SerializationUtils.clone(matchStrategy.getStatsModel()));

        rv = findViewById(R.id.live_match_score_list);
        winnerSelection = findViewById(R.id.winner);
        servingPlayer = findViewById(R.id.servingPlayer);
        matchStatus = findViewById(R.id.matchstatus);
        serveradio = findViewById(R.id.serveradio);
        player1stats = findViewById(R.id.player1stats);
        player2stats = findViewById(R.id.player2stats);
        undoButton = findViewById(R.id.undo_button);
        undoButton.setEnabled(false);
        submit = findViewById(R.id.submit);

        player1Name = findViewById(R.id.player1);
        player1Name.setText(statsModel.getPlayer1ScoreStatsModel().getName());
        player2Name = findViewById(R.id.player2);
        player2Name.setText(statsModel.getPlayer2ScoreStatsModel().getName());

        matchSchedule = findViewById(R.id.matchschedule);
        matchSchedule.setText(player1Name.getText() + " : "+ statsModel.getPlayer1ScoreStatsModel().getAitaId() + " Vs " + player2Name.getText() + " : "+ statsModel.getPlayer2ScoreStatsModel().getAitaId());

        patternValue.put(TID, statsModel.getTid());
        patternValue.put(MID, statsModel.getMid());

        List<String> spinnerArray =  new ArrayList<>();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerArray.add(matchStrategy.getStatsModel().getPlayer1ScoreStatsModel().getName());
        spinnerArray.add(matchStrategy.getStatsModel().getPlayer2ScoreStatsModel().getName());
        //Spinner sItems =  findViewById(R.id.servingplayer);
        servingPlayer.setAdapter(adapter);

        List<String> winnerSelectionPlayers = new ArrayList<>();
        ArrayAdapter<String> winAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, winnerSelectionPlayers);
        winAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        winAdapter.add("-");
        winAdapter.add(matchStrategy.getStatsModel().getPlayer1ScoreStatsModel().getName());
        winAdapter.add(matchStrategy.getStatsModel().getPlayer2ScoreStatsModel().getName());
        winnerSelection.setAdapter(winAdapter);

        servingPlayer.setSelection("player1".equalsIgnoreCase(statsModel.getServingPlayer())?0:1);
        setWinnerSelection();
        boolean completed = false;
        if("Completed".equalsIgnoreCase(statsModel.getStatus()) || "Bye".equalsIgnoreCase(statsModel.getWinStrategy()) || "Walkover".equalsIgnoreCase(statsModel.getWinStrategy())) {
            completed = true;
        }
        matchStatus.setSelection(completed ? 2: "InProgress".equalsIgnoreCase(matchStrategy.getMatchStatus()) || "scheduled".equalsIgnoreCase(matchStrategy.getMatchStatus())?0:1);

        serveradio.clearCheck();
        player1stats.clearCheck();
        player2stats.clearCheck();

        updateView(serveradio,true);
        updateView(player1stats, false);
        updateView(player2stats, false);

        if("Completed".equalsIgnoreCase(statsModel.getStatus())) {

//            serveradio.clearCheck();
//            player1stats.clearCheck();
//            player2stats.clearCheck();
            updateView(serveradio,false);
//            updateView(player1stats, false);
//            updateView(player2stats, false);
            //servingPlayer.setSelection(matchStrategy.getServingPlayer().equals("player1")?0:1);

            findViewById(R.id.winner_text).setVisibility(View.VISIBLE);
            findViewById(R.id.winner).setVisibility(View.VISIBLE);
            findViewById(R.id.winner).setEnabled(false);
            findViewById(R.id.winner_text).setEnabled(false);

            matchStatus.setEnabled(false);
            submit.setEnabled(false);
            //matchStrategy.getStatsModel().setWinner(matchStrategy.getPlayer());
        }
        handleClicks();

        getMatchFormat();

    }

    private void setWinnerSelection() {
        if("player1".equalsIgnoreCase(statsModel.getWinner())) {
            winnerSelection.setSelection(1);
        } else if ( "player2".equalsIgnoreCase(statsModel.getWinner())) {
            winnerSelection.setSelection(2);
        } else {
            winnerSelection.setSelection(0);
        }
    }

    private void populateScores() {
        RecyclerView rv = findViewById(R.id.live_match_score_list);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setItemAnimator(new DefaultItemAnimator());
        AdminLiveScoreAdapter adapter  = new AdminLiveScoreAdapter(this, matchStrategy, matchFormat);
        rv.setAdapter(adapter);

    }

    private void getMatchFormat() {


        ConnectionManager.getSharedInstance().doGet(ConnectionTask.obtainGetTask(APINode.MATCH_FORMAT, this, null, patternValue), new ConnectionManager.ConnectionTaskListenerAdapter() {
            @Override
            public void onConnectionComplete(ConnectionTask task, String json) {
                try {
                    super.onConnectionComplete(task, json);
                } catch (JSONException e) {
                    e.printStackTrace();

                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
                if(task.response!=null)
                    matchFormat =  (MatchFormat)(task.response.items.get(0));
                else {
                    matchFormat =  new MatchFormat();
                    matchFormat.setBestOfNgamesPerMatch("15");
                    matchFormat.setBestOfNSetsPerMatch("1");
                    matchFormat.setIsMatchSetFormat("true");
                    matchFormat.setNumPointsPerTieBreak("10");
                    matchFormat.setSinglePointDeuce("true");

                }

                String matchFormatString = "";

                if("true".equalsIgnoreCase(matchFormat.isSinglePointDeuce())) {
                    matchFormatString = matchFormatString + "Deuce: 1 point,";
                } else
                    matchFormatString = matchFormatString + "Deuce: 2 points,";

                if("true".equalsIgnoreCase(matchFormat.getIsMatchSetFormat())) {
                    matchFormatString = matchFormatString + " Best of " + matchFormat.getBestOfNSetsPerMatch() + " sets, ";
                } else {
                    matchFormatString = matchFormatString + " Best of " + matchFormat.getBestOfNgamesPerMatch() + " games, ";
                }

                matchFormatString = matchFormatString + " Number of points per tiebreak: " + matchFormat.getNumPointsPerTieBreak();

                ((TextView)findViewById(R.id.matchformat)).setText(matchFormatString);

                ListModel<SetModel> player1sets = statsModel.getPlayer1ScoreStatsModel().getSetList();
                ListModel<SetModel> player2sets = statsModel.getPlayer2ScoreStatsModel().getSetList();
                int latestSet = player1sets.items.size()-1;

                int currentGameScoreP1 = Integer.parseInt(player1sets.items.get(latestSet).getGameScore().trim());
                int currentGameScoreP2 = Integer.parseInt(player2sets.items.get(latestSet).getGameScore().trim());

                if("true".equalsIgnoreCase(matchFormat.getIsMatchSetFormat())) {
                    int gamesToWinPerSet = 6;
                    if(currentGameScoreP1 == currentGameScoreP2 && currentGameScoreP1 == gamesToWinPerSet) {
                        matchStrategy.setTieBreakOn(true);
                    }
                } else {
                    int gamesToWinPerMatch = Integer.parseInt(matchFormat.getBestOfNgamesPerMatch())/2 + 1;
                    if(currentGameScoreP1 == currentGameScoreP2 && currentGameScoreP1 == gamesToWinPerMatch) {
                        matchStrategy.setTieBreakOn(true);
                    }
                }

                populateScores();

                //matchStrategy = new MatchStrategy(matchFormat, statsModel);
                matchStrategy.setMatchFormat(matchFormat);
                //matchStrategy.initialiseScoreStructure();



            }
        });

    }

    private String getWinner() {
        Spinner winner = findViewById(R.id.winner);
        String playerName = winner.getSelectedItem().toString();
        if(playerName.equals(matchStrategy.getStatsModel().getPlayer1ScoreStatsModel().getName())) {
            return "player1";
        } else if (playerName.equals(matchStrategy.getStatsModel().getPlayer2ScoreStatsModel().getName())) {
            return "player2";
        }
        return  "";
    }

    private String getMatchStatus() {
        Spinner matchStatusSpinner = findViewById(R.id.matchstatus);
        return matchStatusSpinner.getSelectedItem().toString();
    }

    private void handleClicks() {


        final Context context = this;
        CreateListeners createListeners = new CreateListeners(context).invoke();
        final View.OnClickListener undoListener = createListeners.getUndoListener();
        final View.OnClickListener submitListener = createListeners.getSubmitListener();
        final AdapterView.OnItemSelectedListener servingPlayerListener = createListeners.getServingPlayerListener();
        final AdapterView.OnItemSelectedListener matchStatusOnItemSelectedListener = createListeners.getMatchStatusOnItemSelectedListener();
        final AdapterView.OnItemSelectedListener winnerSelectedListener = createListeners.getWinnerSelectedListener();
        final RadioGroup.OnCheckedChangeListener serverRadioOnCheckedChangedListener = createListeners.getServerRadioOnCheckedChangedListener();
        final RadioGroup.OnCheckedChangeListener player1StatsListener = createListeners.getPlayer1StatsListener();
        final RadioGroup.OnCheckedChangeListener player2StatsOnCheckedChangeListener = createListeners.getPlayer2StatsOnCheckedChangeListener();

        clearListeners();


        registerListeners(undoListener, submitListener, servingPlayerListener, matchStatusOnItemSelectedListener, winnerSelectedListener, serverRadioOnCheckedChangedListener, player1StatsListener, player2StatsOnCheckedChangeListener);
    }

    private void registerListeners(View.OnClickListener undoListener, View.OnClickListener submitListener, AdapterView.OnItemSelectedListener servingPlayerListener, AdapterView.OnItemSelectedListener matchStatusOnItemSelectedListener, AdapterView.OnItemSelectedListener winnerSelectedListener, RadioGroup.OnCheckedChangeListener serverRadioOnCheckedChangedListener, RadioGroup.OnCheckedChangeListener player1StatsListener, RadioGroup.OnCheckedChangeListener player2StatsOnCheckedChangeListener) {
        undoButton.setOnClickListener(undoListener);
        submit.setOnClickListener(submitListener);
        servingPlayer.setOnItemSelectedListener(servingPlayerListener);
        matchStatus.setOnItemSelectedListener(matchStatusOnItemSelectedListener);
        winnerSelection.setOnItemSelectedListener(winnerSelectedListener);
        serveradio.setOnCheckedChangeListener(serverRadioOnCheckedChangedListener);
        player1stats.setOnCheckedChangeListener(player1StatsListener);
        player2stats.setOnCheckedChangeListener(player2StatsOnCheckedChangeListener);
    }

    private void clearListeners() {
        undoButton.setOnClickListener(null);
        submit.setOnClickListener(null);
        servingPlayer.setOnItemSelectedListener(null);
        matchStatus.setOnItemSelectedListener(null);
        serveradio.setOnCheckedChangeListener(null);
        player1stats.setOnCheckedChangeListener(null);
        player2stats.setOnCheckedChangeListener(null);
        winnerSelection.setOnItemSelectedListener(null);
    }

    private void updateViews(RadioGroup serveradio, RadioGroup player1stats, RadioGroup player2stats, boolean enableFlag) {
        updateView(serveradio, enableFlag);
        updateView(player1stats, enableFlag);
        updateView(player2stats, enableFlag);
    }

    private void updateView(RadioGroup rg, boolean enableFlag) {
        for(int i = 0; i < rg.getChildCount(); i++){
            rg.getChildAt(i).setEnabled(enableFlag);
        }
    }

    private void submitMatchStats() {
        matchStrategy.setSubmittedStatsModel(matchStrategy.getStatsModel());
    }

    private void restoreOldMatchStats() {
        matchStrategy.setStatsModel(SerializationUtils.clone(matchStrategy.getSubmittedStatsModel()));
    }


    private class CreateListeners {
        private Context context;
        private View.OnClickListener undoListener;
        private View.OnClickListener submitListener;
        private AdapterView.OnItemSelectedListener servingPlayerListener;
        private AdapterView.OnItemSelectedListener matchStatusOnItemSelectedListener;
        private AdapterView.OnItemSelectedListener winnerSelectedListener;
        private RadioGroup.OnCheckedChangeListener serverRadioOnCheckedChangedListener;
        private RadioGroup.OnCheckedChangeListener player1StatsListener;
        private RadioGroup.OnCheckedChangeListener player2StatsOnCheckedChangeListener;

        public CreateListeners(Context context) {
            this.context = context;
        }

        public View.OnClickListener getSubmitListener() {
            return submitListener;
        }

        public View.OnClickListener getUndoListener() {
            return undoListener;
        }

        public AdapterView.OnItemSelectedListener getWinnerSelectedListener() {
            return winnerSelectedListener;
        }

        public AdapterView.OnItemSelectedListener getServingPlayerListener() {
            return servingPlayerListener;
        }

        public AdapterView.OnItemSelectedListener getMatchStatusOnItemSelectedListener() {
            return matchStatusOnItemSelectedListener;
        }

        public RadioGroup.OnCheckedChangeListener getServerRadioOnCheckedChangedListener() {
            return serverRadioOnCheckedChangedListener;
        }

        public RadioGroup.OnCheckedChangeListener getPlayer1StatsListener() {
            return player1StatsListener;
        }

        public RadioGroup.OnCheckedChangeListener getPlayer2StatsOnCheckedChangeListener() {
            return player2StatsOnCheckedChangeListener;
        }

        public CreateListeners invoke() {

            submitListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if("Completed".equalsIgnoreCase(matchStrategy.getStatsModel().getStatus())) {


                        if("player1".equalsIgnoreCase(getWinner())) {
                            matchStrategy.getStatsModel().setWinner("player1");
                        } else if ("player2".equalsIgnoreCase(getWinner())) {
                            matchStrategy.getStatsModel().setWinner("player2");
                        } else {
                            Toast.makeText(getApplicationContext(),"Please select winner",Toast.LENGTH_LONG).show();
                            return;
                        }

                        clearListeners();

                        serveradio.clearCheck();
                        player1stats.clearCheck();
                        player2stats.clearCheck();
                        updateView(serveradio,false);
                        updateView(player1stats, false);
                        updateView(player2stats, false);

                        Spinner matchStatus = findViewById(R.id.matchstatus);
                        matchStatus.setEnabled(false);
                        winnerSelection.setEnabled(false);
                        servingPlayer.setEnabled(false);
                        undoButton.setEnabled(false);
                        submit.setEnabled(false);
                        UploadStats();
                        return;
                    }

                    clearListeners();

                    serveradio.clearCheck();
                    player1stats.clearCheck();
                    player2stats.clearCheck();

                    updateView(serveradio,true);
                    updateView(player1stats, false);
                    updateView(player2stats, false);


                    matchStrategy.setSubmittedStatsModel(SerializationUtils.clone((matchStrategy.getStatsModel())));
                    //matchStrategy.getStatsModel().setServingPlayer(getServingPlayer(servingPlayer.getSelectedItem().toString()));
                    //matchStrategy.getStatsModel().setStatus(getMatchStatus());

                    servingPlayer.setSelection("player1".equalsIgnoreCase(matchStrategy.getServingPlayer())?0:1);
                    matchStatus.setSelection(statsModel.getStatus().equalsIgnoreCase("Completed") ? 2: matchStrategy.getMatchStatus().equalsIgnoreCase("InProgress")|| matchStrategy.getMatchStatus().equalsIgnoreCase("scheduled")?0:1);
                    undoButton.setEnabled(false);

                    statsModel = matchStrategy.getStatsModel();
                    rv.getAdapter().notifyDataSetChanged();

                    UploadStats();
                    registerListeners(undoListener, submitListener, servingPlayerListener, matchStatusOnItemSelectedListener, winnerSelectedListener, serverRadioOnCheckedChangedListener, player1StatsListener, player2StatsOnCheckedChangeListener);

                }

                private void UploadStats() {
                    try {
                        ConnectionManager.getSharedInstance().doPost(ConnectionTask.obtainPOSTTask(APINode.LIVE_SCORE_STATS_POST, context, null, patternValue, matchStrategy.getStatsModel()), new ConnectionManager.ConnectionTaskListenerAdapter() {
                            @Override
                            public void onConnectionComplete(ConnectionTask task, String json) {
                                try {
                                    super.onConnectionComplete(task, json);

                                } catch (JSONException e) {
                                    e.printStackTrace();

                                } catch (IllegalAccessException e) {
                                    e.printStackTrace();
                                } catch (InstantiationException e) {
                                    e.printStackTrace();
                                } catch (NoSuchMethodException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (InvocationTargetException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                }
            };


            undoListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clearListeners();
                    restoreOldMatchStats();
                    undoButton.setEnabled(false);

                    serveradio.clearCheck();
                    player1stats.clearCheck();
                    player2stats.clearCheck();


                    updateView(serveradio, true);
                    updateView(player1stats, false);
                    updateView(player2stats, false);
                    rv.getAdapter().notifyDataSetChanged();
                    servingPlayer.setSelection("player1".equalsIgnoreCase(matchStrategy.getServingPlayer())?0:1);
                    matchStatus.setSelection("Completed".equalsIgnoreCase(statsModel.getStatus()) ? 2: "InProgress".equalsIgnoreCase(matchStrategy.getMatchStatus())|| "scheduled".equalsIgnoreCase(matchStrategy.getMatchStatus())?0:1);
                    registerListeners(undoListener, submitListener , servingPlayerListener, matchStatusOnItemSelectedListener, winnerSelectedListener, serverRadioOnCheckedChangedListener, player1StatsListener, player2StatsOnCheckedChangeListener);


                }
            };

            servingPlayerListener = new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    //undoButton.setEnabled(true);
                    String value = parent.getItemAtPosition(position).toString();
                    StatsModel statsModel = matchStrategy.getStatsModel();
                    if (value.equalsIgnoreCase(matchStrategy.getStatsModel().getPlayer1ScoreStatsModel().getName()))
                        statsModel.setServingPlayer("player1");
                    else
                        statsModel.setServingPlayer("player2");
                    matchStrategy.setStatsModel(statsModel);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            };


            winnerSelectedListener = new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String value = parent.getItemAtPosition(position).toString();
                    StatsModel statsModel = matchStrategy.getStatsModel();
                    if (value.equalsIgnoreCase(statsModel.getPlayer1ScoreStatsModel().getName())) {
                        statsModel.setWinner("player1");
                    } else if (value.equalsIgnoreCase(statsModel.getPlayer2ScoreStatsModel().getName())) {
                        statsModel.setWinner("player2");
                    }
                    matchStrategy.setStatsModel(statsModel);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            };

            matchStatusOnItemSelectedListener = new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    //undoButton.setEnabled(true);
                    String value = parent.getItemAtPosition(position).toString();
                    if("Completed".equalsIgnoreCase(value) || "Bye".equalsIgnoreCase(value) || "Walkover".equalsIgnoreCase(value)) {
                        matchStrategy.getStatsModel().setStatus("Completed");
                        matchStrategy.getStatsModel().setWinStrategy(value);
                        findViewById(R.id.winner_text).setVisibility(View.VISIBLE);
                        findViewById(R.id.winner).setVisibility(View.VISIBLE);
                    } else {
                        matchStrategy.setMatchStatus(value);
                        findViewById(R.id.winner_text).setVisibility(View.GONE);
                        findViewById(R.id.winner).setVisibility(View.GONE);
                    }
                    //statsModel = matchStrategy.getStatsModel();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            };


            serverRadioOnCheckedChangedListener = new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    undoButton.setEnabled(true);
                    updateView(player1stats, true);
                    updateView(player2stats, true);
                    updateView(serveradio, false);
                    String player;
                    if (servingPlayer.getSelectedItem().toString().equals(matchStrategy.getStatsModel().getPlayer1ScoreStatsModel().getName())) {
                        findViewById(R.id.ace2).setEnabled(false);
                        player = "player1";
                    } else {
                        findViewById(R.id.ace1).setEnabled(false);
                        player = "player2";
                    }


                    switch (checkedId) {
                        case R.id.firstserve:
                            matchStrategy.setServeStrategy("first");
                            break;
                        case R.id.secondserve:
                            matchStrategy.setServeStrategy("second");
                            break;
                        case R.id.doublefault:
                            matchStrategy.setPlayer(player.equals("player1")?"player2":"player1");
                            matchStrategy.setServeStrategy("doublefault");
                            matchStrategy.calculateStats();
                            matchStrategy.calculateScore();
                            matchStrategy.CalculatePostScoreStats();
                            statsModel = matchStrategy.getStatsModel();
                            rv.getAdapter().notifyDataSetChanged();
                            updateViews(serveradio, player1stats, player2stats, false);
                            break;
                    }
                }
            };


            player1StatsListener = new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    undoButton.setEnabled(true);

                    switch (checkedId) {
                        case R.id.ace1:
                            matchStrategy.setPointStrategy("ace");
                            matchStrategy.setPlayer("player1");
                            break;
                        case R.id.winner1:
                            matchStrategy.setPointStrategy("winner");
                            matchStrategy.setPlayer("player1");
                            break;
                        case R.id.error1:
                            matchStrategy.setPointStrategy("error");
                            matchStrategy.setPlayer("player2");
                            break;
                    }
                    matchStrategy.calculateStats();
                    matchStrategy.calculateScore();
                    matchStrategy.CalculatePostScoreStats();
                    statsModel = matchStrategy.getStatsModel();
                    rv.getAdapter().notifyDataSetChanged();
                    servingPlayer.setSelection("player1".equalsIgnoreCase(matchStrategy.getServingPlayer())?0:1);
                    matchStatus.setSelection("Completed".equalsIgnoreCase(statsModel.getStatus()) ? 2: "InProgress".equalsIgnoreCase(matchStrategy.getMatchStatus())|| "scheduled".equalsIgnoreCase(matchStrategy.getMatchStatus())?0:1);
                    if("Completed".equalsIgnoreCase(statsModel.getStatus())) {
                        setWinnerSelection();
                    }
                    updateViews(serveradio, player1stats, player2stats, false);
                }
            };


            player2StatsOnCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    undoButton.setEnabled(true);
                    switch (checkedId) {
                        case R.id.ace2:
                            matchStrategy.setPointStrategy("ace");
                            matchStrategy.setPlayer("player2");
                            break;
                        case R.id.winner2:
                            matchStrategy.setPointStrategy("winner");
                            matchStrategy.setPlayer("player2");
                            break;
                        case R.id.error2:
                            matchStrategy.setPointStrategy("error");
                            matchStrategy.setPlayer("player1");
                            break;
                    }
                    matchStrategy.calculateStats();
                    matchStrategy.calculateScore();
                    matchStrategy.CalculatePostScoreStats();
                    statsModel = matchStrategy.getStatsModel();
                    rv.getAdapter().notifyDataSetChanged();
                    servingPlayer.setSelection("player1".equalsIgnoreCase(matchStrategy.getServingPlayer())?0:1);
                    matchStatus.setSelection("Completed".equalsIgnoreCase(statsModel.getStatus()) ? 2: "InProgress".equalsIgnoreCase(matchStrategy.getMatchStatus())|| "scheduled".equalsIgnoreCase(matchStrategy.getMatchStatus())?0:1);
                    if("Completed".equalsIgnoreCase(statsModel.getStatus())) {
                        setWinnerSelection();
                    }
                    updateViews(serveradio, player1stats, player2stats, false);
                }
            };
            return this;
        }
    }
}
