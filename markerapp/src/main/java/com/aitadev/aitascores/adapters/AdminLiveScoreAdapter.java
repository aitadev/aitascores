package com.aitadev.aitascores.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.aitadev.aitascores.strategy.MatchStrategy;
import com.aitadev.sdk.aitamodels.ListModel;
import com.aitadev.sdk.aitamodels.MatchFormat;
import com.aitadev.sdk.aitamodels.PlayerScoreStatsModel;
import com.aitadev.sdk.aitamodels.SetModel;
import com.aitascores.shsadago.aitascores.admin.R;

import java.util.ArrayList;
import java.util.List;

public class AdminLiveScoreAdapter extends RecyclerView.Adapter<AdminLiveScoreViewHolder> {

    private MatchFormat matchFormat;
    private MatchStrategy matchStrategy;
    private Context context;
    private List<String> setScoreArray;
    private ArrayAdapter<String> pointScoreArrayAdapter1;
    private ArrayAdapter<String> pointScoreArrayAdapter2;

    private ArrayAdapter<String> setScoreArrayAdapter;
    private int rvPos;

    public AdminLiveScoreAdapter(Context context, MatchStrategy matchStrategy, MatchFormat matchFormat) {
        this.context = context;
        this.matchStrategy = matchStrategy;
        this.matchFormat = matchFormat;

        createGameScoreAdapter();

    }

    private void createGameScoreAdapter() {

        pointScoreArrayAdapter1 = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, new ArrayList<String>());
        pointScoreArrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        pointScoreArrayAdapter2 = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, new ArrayList<String>());
        pointScoreArrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        setPointScoreArray();

        setScoreArray = new ArrayList<>();
        setScoreArrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, setScoreArray);
        setScoreArray.add("");
        if("true".equals(matchFormat.getIsMatchSetFormat())) {
            for (int i=0;i<=7;i++) {
                setScoreArray.add(String.valueOf(i));
            }
        } else {
            for(int i=0;i<=(Integer.parseInt(matchFormat.getBestOfNgamesPerMatch())/2)+1;i++) {
                setScoreArray.add(String.valueOf(i));
            }
        }
    }

    private void setPointScoreArray() {
        pointScoreArrayAdapter1.clear();
        pointScoreArrayAdapter2.clear();
        pointScoreArrayAdapter1.add("0");
        pointScoreArrayAdapter1.add("15");
        pointScoreArrayAdapter1.add("30");
        pointScoreArrayAdapter1.add("40");

        if("false".equals(matchFormat.isSinglePointDeuce())){
            pointScoreArrayAdapter1.add("A");
            pointScoreArrayAdapter1.add("-");
        }


        pointScoreArrayAdapter2.add("0");
        pointScoreArrayAdapter2.add("15");
        pointScoreArrayAdapter2.add("30");
        pointScoreArrayAdapter2.add("40");

        if("false".equals(matchFormat.isSinglePointDeuce())){
            pointScoreArrayAdapter2.add("A");
            pointScoreArrayAdapter2.add("-");
        }
    }

    @NonNull
    @Override
    public AdminLiveScoreViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //INFLATE A VIEW FROM XML
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.admin_match_live_score_layout, null);

        //HOLDER
        AdminLiveScoreViewHolder holder = new AdminLiveScoreViewHolder(v);

        handleClicks(holder);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull AdminLiveScoreViewHolder holder, int position) {
        rvPos = position;

        if(matchStrategy.isTieBreakOn()) {
            ListModel<SetModel> player1SetData = matchStrategy.getStatsModel().getPlayer1ScoreStatsModel().getSetList();
            ListModel<SetModel> player2SetData = matchStrategy.getStatsModel().getPlayer2ScoreStatsModel().getSetList();

            int latestSet = player1SetData.items.size() - 1;

            pointScoreArrayAdapter1.clear();
            int score = Integer.parseInt(player1SetData.items.get(latestSet).getPointScore());
            int sIndex = score <= 2 ? 0 : score - 2;
            int eIndex = score + 2;
            for(int i=sIndex;i<=eIndex;i++) {
                pointScoreArrayAdapter1.add(String.valueOf(i));
            }
            pointScoreArrayAdapter1.notifyDataSetChanged();


            pointScoreArrayAdapter2.clear();
            score = Integer.parseInt(player2SetData.items.get(latestSet).getPointScore());
            sIndex = score <= 2 ? 0 : score - 2;
            eIndex = score + 2;
            for(int i=sIndex;i<=eIndex;i++) {
                pointScoreArrayAdapter2.add(String.valueOf(i));
            }
            pointScoreArrayAdapter2.notifyDataSetChanged();
        } else
            setPointScoreArray();


        if (position == 0) {
            holder.editPoint.setAdapter(pointScoreArrayAdapter1);
           // pointScoreArrayAdapter1.notifyDataSetChanged();
        } else {
            holder.editPoint.setAdapter(pointScoreArrayAdapter2);
            //pointScoreArrayAdapter2.notifyDataSetChanged();
        }

        holder.matchScore.setAdapter(setScoreArrayAdapter);
        holder.set1.setAdapter(setScoreArrayAdapter);
        holder.set2.setAdapter(setScoreArrayAdapter);
        holder.set3.setAdapter(setScoreArrayAdapter);





        ListModel<SetModel> player1SetData = matchStrategy.getStatsModel().getPlayer1ScoreStatsModel().getSetList();
        ListModel<SetModel> player2SetData = matchStrategy.getStatsModel().getPlayer2ScoreStatsModel().getSetList();

        int latestSet = player1SetData.items.size() - 1;

        if(position==0) {
            if("player1".equalsIgnoreCase(matchStrategy.getServingPlayer())) {
                holder.status.setText("*");
            } else {
                holder.status.setText("");
            }
            setSelections(holder, pointScoreArrayAdapter1, player1SetData, matchStrategy.getStatsModel().getPlayer1ScoreStatsModel(), latestSet);
        }
        if(position==1) {
            if("player2".equalsIgnoreCase(matchStrategy.getServingPlayer())) {
                holder.status.setText("*");
            } else {
                holder.status.setText("");
            }
            setSelections(holder, pointScoreArrayAdapter2, player2SetData, matchStrategy.getStatsModel().getPlayer2ScoreStatsModel(), latestSet);
        }

    }

    private void setSelections(@NonNull AdminLiveScoreViewHolder holder, ArrayAdapter<String> pointScoreArrayAdapter, ListModel<SetModel> playerSetData, PlayerScoreStatsModel playerScoreStatsModel, int latestSet) {


        holder.player.setText(playerScoreStatsModel.getName());
        holder.editPoint.setSelection(pointScoreArrayAdapter.getPosition(playerSetData.items.get(latestSet).getPointScore()));
        holder.matchScore.setSelection(setScoreArrayAdapter.getPosition(getWonSets(playerScoreStatsModel)));

        for(int i=0;i<=latestSet;i++) {
            if(i==0){
                holder.set1.setSelection(setScoreArrayAdapter.getPosition(playerSetData.items.get(0).getGameScore()));
            }
            if(i==1){
                if(playerSetData.items.get(1) != null) holder.set2.setSelection(setScoreArrayAdapter.getPosition(playerSetData.items.get(1).getGameScore()));
            }
            if(i==2){
                if(playerSetData.items.get(2) != null) holder.set3.setSelection(setScoreArrayAdapter.getPosition(playerSetData.items.get(2).getGameScore()));
            }
        }
    }

    private String getWonSets(PlayerScoreStatsModel player) {
        int numSets = player.getSetList().items.size();
        int numWins = 0;
        for(int i=0;i<numSets;i++) {
            if("W".equals(player.getSetList().items.get(i).getResult())) {
                numWins++;
            }
        }
        return String.valueOf(numWins);

    }

    private void handleClicks(@NonNull AdminLiveScoreViewHolder holder) {
        final ListModel<SetModel> player1SetData = matchStrategy.getStatsModel().getPlayer1ScoreStatsModel().getSetList();
        final ListModel<SetModel> player2SetData = matchStrategy.getStatsModel().getPlayer2ScoreStatsModel().getSetList();
        final int latestSet = player1SetData.items.size() - 1;
        holder.editPoint.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(rvPos==0)
                    player1SetData.items.get(latestSet).setPointScore(parent.getItemAtPosition(position).toString());
                else if(rvPos==1)
                    player2SetData.items.get(latestSet).setPointScore(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        holder.set1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(rvPos==0)
                    player1SetData.items.get(0).setGameScore(parent.getItemAtPosition(position).toString());
                else if(rvPos==1)
                    player2SetData.items.get(0).setGameScore(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        holder.set2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if((player1SetData.items.size()-1)>=1){
                    if(rvPos==0)
                        player1SetData.items.get(1).setGameScore(parent.getItemAtPosition(position).toString());
                    else if(rvPos==1)
                        player2SetData.items.get(1).setGameScore(parent.getItemAtPosition(position).toString());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        holder.set3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if((player1SetData.items.size()-1)>=2){
                    if(rvPos==0)
                        player1SetData.items.get(2).setGameScore(parent.getItemAtPosition(position).toString());
                    else if(rvPos==1)
                        player2SetData.items.get(2).setGameScore(parent.getItemAtPosition(position).toString());
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
