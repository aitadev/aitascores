package com.aitadev.aitascores.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import com.aitascores.shsadago.aitascores.admin.R;


public class AdminLiveScoreViewHolder extends RecyclerView.ViewHolder {

    public Spinner set1, set2, set3, matchScore, editPoint;
    public TextView player,status;

    public AdminLiveScoreViewHolder(View itemView) {
        super(itemView);
        this.player = (TextView) itemView.findViewById(R.id.player);
        this.set1 = (Spinner) itemView.findViewById(R.id.set1);
        this.set2 = (Spinner) itemView.findViewById(R.id.set2);
        this.set3 = (Spinner) itemView.findViewById(R.id.set3);
        this.matchScore = (Spinner) itemView.findViewById(R.id.matchscore);
        this.editPoint = (Spinner) itemView.findViewById(R.id.editpoint);
        this.status = (TextView) itemView.findViewById(R.id.status);

    }


}
