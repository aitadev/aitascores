package com.aitadev.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aitadev.sdk.aitamodels.PlayerScoreStatsModel;
import com.aitadev.sdk.aitamodels.StatsModel;
import com.aitadev.sdk.aitamodels.WrapperStatsModel;
import com.aitascores.shsadago.aitascores.R;

public class MatchStatsListAdapter extends RecyclerView.Adapter<MatchStatsViewHolder> {

    WrapperStatsModel wrapperStatsModel;
    private Context context;

    public MatchStatsListAdapter(Context context, WrapperStatsModel wrapperStatsModel) {
        this.context = context;
        this.wrapperStatsModel = wrapperStatsModel;
    }

    @NonNull
    @Override
    public MatchStatsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //INFLATE A VIEW FROM XML
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.match_stats_table_item_layout, null);

        //HOLDER
        MatchStatsViewHolder holder = new MatchStatsViewHolder(v, context);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MatchStatsViewHolder holder, int position) {

        if(wrapperStatsModel.getStatsModel()==null)
            return;
        PlayerScoreStatsModel player1 = wrapperStatsModel.getStatsModel().getPlayer1ScoreStatsModel();
        PlayerScoreStatsModel player2 = wrapperStatsModel.getStatsModel().getPlayer2ScoreStatsModel();

        switch (position) {
            case 0:
                holder.statPlayer1.setText(player1.getMatchStatModel().getAces());
                holder.statName.setText("Aces");
                holder.statPlayer2.setText(player2.getMatchStatModel().getAces());
                break;
            case 1:
                holder.statPlayer1.setText(player1.getMatchStatModel().getDoubleFaults());
                holder.statName.setText("Double Faults");
                holder.statPlayer2.setText(player2.getMatchStatModel().getDoubleFaults());
                break;
            case 2:
                holder.statPlayer1.setText(player1.getMatchStatModel().getErrors());
                holder.statName.setText("Errors");
                holder.statPlayer2.setText(player2.getMatchStatModel().getErrors());
                break;
            case 3:
                holder.statPlayer1.setText(player1.getMatchStatModel().getFirstServeWon());
                holder.statName.setText("1st Serves Won");
                holder.statPlayer2.setText(player2.getMatchStatModel().getFirstServeWon());
                break;
            case 4:
                holder.statPlayer1.setText(player1.getMatchStatModel().getTotalFirstServe());
                holder.statName.setText("Total First Serves");
                holder.statPlayer2.setText(player2.getMatchStatModel().getTotalFirstServe());
                break;
            case 5:
                holder.statPlayer1.setText(player1.getMatchStatModel().getSecondServeWon());
                holder.statName.setText("2nd Serves Won");
                holder.statPlayer2.setText(player2.getMatchStatModel().getSecondServeWon());
                break;
            case 6:
                holder.statPlayer1.setText(player1.getMatchStatModel().getTotalSecondServe());
                holder.statName.setText("Total Second Serves");
                holder.statPlayer2.setText(player2.getMatchStatModel().getTotalSecondServe());
                break;
            case 7:
                holder.statPlayer1.setText(player1.getMatchStatModel().getWinners());
                holder.statName.setText("Winners");
                holder.statPlayer2.setText(player2.getMatchStatModel().getWinners());
                break;
            case 8:
                holder.statPlayer1.setText(player1.getMatchStatModel().getBreakPointsFaced());
                holder.statName.setText("Breakpoints Faced");
                holder.statPlayer2.setText(player2.getMatchStatModel().getBreakPointsFaced());
                break;
            case 9:
                holder.statPlayer1.setText(player1.getMatchStatModel().getBreakPointsConverted());
                holder.statName.setText("Breakpoints Converted");
                holder.statPlayer2.setText(player2.getMatchStatModel().getBreakPointsConverted());
                break;
             default:
                 break;

        }
    }

    @Override
    public int getItemCount() {
        return 10;//wrapperStatsModel.getStatsModel().getPlayer1ScoreStatsModel().getClass().getDeclaredFields().length - 2;
    }
}
