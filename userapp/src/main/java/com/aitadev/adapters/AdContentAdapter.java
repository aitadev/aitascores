package com.aitadev.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aitadev.sdk.aitamodels.AdModel;
import com.aitadev.sdk.aitamodels.AdModelUI;
import com.aitadev.sdk.aitamodels.ListModel;
import com.aitascores.shsadago.aitascores.R;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

public class AdContentAdapter extends RecyclerView.Adapter<AdContentViewHolder> {

    ListModel<AdModelUI> adContent;
    private Context context;

    private static HashMap<String,String> adKeysLookup;
    static {
        adKeysLookup = new HashMap<>();
        adKeysLookup.put("feeStructure","Fee Structure");
        adKeysLookup.put("headCoach","Head Coach");
        adKeysLookup.put("headCoachContact","Head Coach Contact");
        adKeysLookup.put("isFitnessAvailable","Fitness Availability");
        adKeysLookup.put("location","Address");
        adKeysLookup.put("numAitaPlayers","Number of AITA Players");
        adKeysLookup.put("numClayCourts","Number of Clay Courts");
        adKeysLookup.put("numCoaches","Number of Coaches");
        adKeysLookup.put("numFloodLights","Number of Flood Lights");
        adKeysLookup.put("numHardCourts","Number of Hard Courts");
        adKeysLookup.put("numMarkers","Number of Markers");
    }

    public AdContentAdapter(Context context, ListModel adContent) {
        this.context = context;
        this.adContent = adContent;
    }

    @NonNull
    @Override
    public AdContentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        //INFLATE A VIEW FROM XML
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ad_content_table_layout, null);

        //HOLDER
        AdContentViewHolder holder = new AdContentViewHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull AdContentViewHolder holder, int i) {
        if (this.adContent == null) {
            String adFieldName = AdModel.class.getDeclaredFields()[i].getName();
            if(!adFieldName.equalsIgnoreCase("aid")) {
                holder.adField.setText(adFieldName.toUpperCase());
                holder.adValue.setText("");
            } else {
                holder.adField.setEnabled(false);
                holder.adValue.setEnabled(false);
            }
            return;
        }
        String adFieldName = this.adContent.items.get(0).getClass().getSuperclass().getDeclaredFields()[i].getName();
        String adValue = "";
        try {
            Method getMethod = this.adContent.items.get(0).getClass().getMethod("get"+capitalizeFirstLetter(adFieldName));
            adValue = (String) getMethod.invoke(this.adContent.items.get(0));
            String adKey = adKeysLookup.get(adFieldName);
            if(adKey != null) {
                holder.adField.setText(adKey);
                holder.adValue.setText(adValue);
            } else {
                holder.adField.setText(adFieldName);
                holder.adValue.setText(adValue);
            }


        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }


    }

    private String capitalizeFirstLetter(String original) {
        if (original == null || original.length() == 0) {
            return original;
        }
        return original.substring(0, 1).toUpperCase() + original.substring(1);
    }


    @Override
    public int getItemCount() {

        int len = AdModel.class != null ? AdModel.class.getDeclaredFields().length : 0;
        return len;
    }

}
