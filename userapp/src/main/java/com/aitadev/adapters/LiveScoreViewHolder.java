package com.aitadev.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.aitadev.aitascores.aitaUx.ItemClickListener;
import com.aitascores.shsadago.aitascores.R;

public class LiveScoreViewHolder extends RecyclerView.ViewHolder {

    public TextView set1,set2,set3,setScore,point,status,player;

    private ItemClickListener itemClickListener;


    public LiveScoreViewHolder(View itemView) {
        super(itemView);
        this.player = (TextView) itemView.findViewById(R.id.player);
        this.status = (TextView) itemView.findViewById(R.id.status);
        this.set1 = (TextView) itemView.findViewById(R.id.set1);
        this.set2 = (TextView) itemView.findViewById(R.id.set2);
        this.set3 = (TextView) itemView.findViewById(R.id.set3);

        this.setScore = (TextView) itemView.findViewById(R.id.setscore);
        this.point = (TextView) itemView.findViewById(R.id.point);

        //itemView.setOnClickListener(this);

    }


//    @Override
//    public void onClick(View v) {
//        this.itemClickListener.onItemClick(v, getLayoutPosition());
//    }
//
//    public void setItemClickListener(ItemClickListener ic) {
//        this.itemClickListener = ic;

//    }

}
