package com.aitadev.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.aitadev.aitascores.aitaUx.ItemClickListener;
import com.aitascores.shsadago.aitascores.R;

public class MatchStatsListItemViewHolder extends RecyclerView.ViewHolder  {

    public TextView statName, statValue;

    private ItemClickListener itemClickListener;


    public MatchStatsListItemViewHolder(View itemView) {
        super(itemView);
        this.statName = (TextView) itemView.findViewById(R.id.stat_name);
        this.statValue = (TextView) itemView.findViewById(R.id.stat_value);

       // itemView.setOnClickListener(this);

    }


//    @Override
//    public void onClick(View v) {
//        this.itemClickListener.onItemClick(v, getLayoutPosition());
//    }
//
//    public void setItemClickListener(ItemClickListener ic) {
//        this.itemClickListener = ic;
//
//    }

}
