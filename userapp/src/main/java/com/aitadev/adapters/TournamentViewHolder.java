package com.aitadev.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.aitadev.aitascores.aitaUx.ItemClickListener;
import com.aitascores.shsadago.aitascores.R;

public class TournamentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    private TextView tournamentNameTextView;
    private ItemClickListener itemClickListener;


    public TournamentViewHolder(View itemView) {
        super(itemView);
        this.tournamentNameTextView = (TextView) itemView.findViewById(R.id.tournamementName);
        itemView.setOnClickListener(this);

    }


    public TextView getTournamentNameTextView() {
        return tournamentNameTextView;
    }

    public void setTournamentNameTextView(TextView tournamentNameTextView) {
        this.tournamentNameTextView = tournamentNameTextView;
    }

    @Override
    public void onClick(View v) {
        this.itemClickListener.onItemClick(v, getLayoutPosition());
    }

    public void setItemClickListener(ItemClickListener ic) {
        this.itemClickListener = ic;

    }

}
