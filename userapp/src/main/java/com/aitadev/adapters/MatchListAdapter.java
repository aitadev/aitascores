package com.aitadev.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aitadev.aitascores.aitaUx.ItemClickListener;
import com.aitadev.aitascores.aitaUx.StatsActivity;
import com.aitadev.sdk.aitamodels.ListModel;
import com.aitadev.sdk.aitamodels.MatchModel;
import com.aitascores.shsadago.aitascores.R;

public class MatchListAdapter extends RecyclerView.Adapter<MatchViewHolder> {

    ListModel<MatchModel> matchModelList;
    private Context context;
    private IntentData intentData;

    public MatchListAdapter(Context context, ListModel matchList, IntentData intentData) {
        this.context = context;
        this.matchModelList = matchList;
        this.intentData = intentData;
    }

    @NonNull
    @Override
    public MatchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //INFLATE A VIEW FROM XML
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.match_list_item, null);

        //HOLDER
        MatchViewHolder holder = new MatchViewHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MatchViewHolder holder, final int position) {

        //BIND DATA
        holder.player1.setText(matchModelList.items.get(position).getPlayer1ScoreModel().getName() + isPlayerServing(position, "player1"));
        holder.set1.setText(matchModelList.items.get(position).getPlayer1ScoreModel().getMatchScore());



        holder.player2.setText(matchModelList.items.get(position).getPlayer2ScoreModel().getName() + isPlayerServing(position, "player2"));
        holder.set2.setText(matchModelList.items.get(position).getPlayer2ScoreModel().getMatchScore());
        //holder.status2.setText("player2".equalsIgnoreCase(matchModelList.items.get(position).getWinner())?"W":"");

        setStatus(holder, position);

        if("player1".equalsIgnoreCase(matchModelList.items.get(position).getWinner()) == true) {
            holder.player1.setTypeface(null, Typeface.BOLD);
            holder.player1.setTextColor(ContextCompat.getColor(context, R.color.green));

            holder.set1.setTypeface(null, Typeface.BOLD);
            holder.set1.setTextColor(ContextCompat.getColor(context, R.color.green));

            holder.game1.setTypeface(null, Typeface.BOLD);
            holder.game1.setTextColor(ContextCompat.getColor(context, R.color.green));

            holder.point1.setTypeface(null, Typeface.BOLD);
            holder.point1.setTextColor(ContextCompat.getColor(context, R.color.green));

            holder.status1.setTypeface(null, Typeface.BOLD);
            holder.status1.setTextColor(ContextCompat.getColor(context, R.color.green));

        } else if ("player2".equalsIgnoreCase(matchModelList.items.get(position).getWinner()) == true) {
            holder.player2.setTypeface(null, Typeface.BOLD);
            holder.player2.setTextColor(ContextCompat.getColor(context, R.color.green));

            holder.set2.setTypeface(null, Typeface.BOLD);
            holder.set2.setTextColor(ContextCompat.getColor(context, R.color.green));

            holder.game2.setTypeface(null, Typeface.BOLD);
            holder.game2.setTextColor(ContextCompat.getColor(context, R.color.green));

            holder.point2.setTypeface(null, Typeface.BOLD);
            holder.point2.setTextColor(ContextCompat.getColor(context, R.color.green));

            holder.status2.setTypeface(null, Typeface.BOLD);
            holder.status2.setTextColor(ContextCompat.getColor(context, R.color.green));
        }

        if("completed".equalsIgnoreCase(matchModelList.items.get(position).getStatus()) == false) {
            if("player1".equalsIgnoreCase(matchModelList.items.get(position).getServingPlayer()) == true) {
                holder.player1.setTypeface(null, Typeface.BOLD);
                holder.player1.setTextColor(ContextCompat.getColor(context, R.color.orange));

                holder.set1.setTypeface(null, Typeface.BOLD);
                holder.set1.setTextColor(ContextCompat.getColor(context, R.color.orange));

                holder.game1.setTypeface(null, Typeface.BOLD);
                holder.game1.setTextColor(ContextCompat.getColor(context, R.color.orange));

                holder.point1.setTypeface(null, Typeface.BOLD);
                holder.point1.setTextColor(ContextCompat.getColor(context, R.color.orange));

                holder.status1.setTypeface(null, Typeface.BOLD);
                holder.status1.setTextColor(ContextCompat.getColor(context, R.color.orange));

            } else if ("player2".equalsIgnoreCase(matchModelList.items.get(position).getServingPlayer()) == true) {
                holder.player2.setTypeface(null, Typeface.BOLD);
                holder.player2.setTextColor(ContextCompat.getColor(context, R.color.orange));

                holder.set2.setTypeface(null, Typeface.BOLD);
                holder.set2.setTextColor(ContextCompat.getColor(context, R.color.orange));

                holder.game2.setTypeface(null, Typeface.BOLD);
                holder.game2.setTextColor(ContextCompat.getColor(context, R.color.orange));

                holder.point2.setTypeface(null, Typeface.BOLD);
                holder.point2.setTextColor(ContextCompat.getColor(context, R.color.orange));

                holder.status2.setTypeface(null, Typeface.BOLD);
                holder.status2.setTextColor(ContextCompat.getColor(context, R.color.orange));
            }
        }


        if(!"completed".equalsIgnoreCase(matchModelList.items.get(position).getStatus())) {
            holder.game1.setText(matchModelList.items.get(position).getPlayer1ScoreModel().getGameScore());
            holder.point1.setText(matchModelList.items.get(position).getPlayer1ScoreModel().getPointScore());
            holder.game2.setText(matchModelList.items.get(position).getPlayer2ScoreModel().getGameScore());
            holder.point2.setText(matchModelList.items.get(position).getPlayer2ScoreModel().getPointScore());
        }

        //WHEN a MATCH IS CLICKED
        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

                if("Bye".equalsIgnoreCase(matchModelList.items.get(position).getWinStrategy()) == false) {
                    //INTENT OBJ
                    Intent i = new Intent(context, StatsActivity.class);

                    //ADD DATA TO OUR INTENT
                    i.putExtra("Tid", intentData.getTid());
                    i.putExtra("TName", intentData.getTname());
                    i.putExtra("Mid", matchModelList.items.get(pos).getMid());
                    i.putExtra("Aid",intentData.getAid());


                    //START DETAIL ACTIVITY
                    context.startActivity(i);
                }


            }
        });

    }

    private void setStatus(@NonNull MatchViewHolder holder, int position) {

        if("Bye".equalsIgnoreCase(matchModelList.items.get(position).getWinStrategy())) {
            if("player1".equalsIgnoreCase(matchModelList.items.get(position).getWinner())) {
                holder.status2.setText("BYE");
            } else {
                holder.status1.setText("BYE");
            }
        } else if ("WalkOver".equalsIgnoreCase(matchModelList.items.get(position).getWinStrategy())) {
            if("player1".equalsIgnoreCase(matchModelList.items.get(position).getWinner())) {
                holder.status2.setText("WO");
            } else {
                holder.status1.setText("WO");
            }
        } else if ("Completed".equalsIgnoreCase(matchModelList.items.get(position).getWinStrategy())) {
            holder.status1.setText("player1".equalsIgnoreCase(matchModelList.items.get(position).getWinner())?"W":"");
            holder.status2.setText("player2".equalsIgnoreCase(matchModelList.items.get(position).getWinner())?"W":"");
        }
    }

    private String isPlayerServing(int pos, String player) {
        return player.equalsIgnoreCase(matchModelList.items.get(pos).getServingPlayer()) == true ? " * ":"";
    }

    @Override
    public int getItemCount() {
        return matchModelList.items.size();
    }
}
