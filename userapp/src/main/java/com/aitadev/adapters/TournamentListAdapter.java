package com.aitadev.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aitadev.aitascores.aitaUx.ItemClickListener;
import com.aitadev.aitascores.aitaUx.MatchActivity;
import com.aitadev.sdk.aitamodels.ListModel;
import com.aitadev.sdk.aitamodels.TournamentModel;
import com.aitascores.shsadago.aitascores.R;

public class TournamentListAdapter extends RecyclerView.Adapter<TournamentViewHolder> {

    ListModel<TournamentModel> tournamentList;
    private Context context;

    public TournamentListAdapter(Context context, ListModel tournamentList) {
        this.context = context;
        this.tournamentList = tournamentList;
    }

    @NonNull
    @Override
    public TournamentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //INFLATE A VIEW FROM XML
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.tournament_list_item, null);

        //HOLDER
        TournamentViewHolder holder = new TournamentViewHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull TournamentViewHolder holder, int position) {
        //BIND DATA
        holder.getTournamentNameTextView().setText(tournamentList.items.get(position).getTname());

        //WHEN ITEM IS CLICKED
        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                Log.d("****sheera*****", "Inside onItemClick");
                //INTENT OBJ
                Intent i = new Intent(context, MatchActivity.class);

                //ADD DATA TO OUR INTENT
                i.putExtra("Tid", tournamentList.items.get(pos).getTid());
                i.putExtra("Aid", tournamentList.items.get(pos).getAid());
                i.putExtra("Tname", tournamentList.items.get(pos).getTname());

                //START DETAIL ACTIVITY
                Log.d("****sheera*****", "Going to start matchactivity");

                context.startActivity(i);

            }
        });
    }

    @Override
    public int getItemCount() {
        return (tournamentList!=null && tournamentList.items!=null) ? tournamentList.items.size() : 0;
    }
}
