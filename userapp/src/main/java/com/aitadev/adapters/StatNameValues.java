package com.aitadev.adapters;

public class StatNameValues {
    private String statName;
    private String statValuePlayer1;
    private String statValuePlayer2;

    public StatNameValues(String name, String player1Value){
        this.statName = name;
        this.statValuePlayer1 = player1Value;
    }

    public String getStatName() {
        return statName;
    }

    public void setStatName(String statName) {
        this.statName = statName;
    }

    public String getStatValuePlayer1() {
        return statValuePlayer1;
    }

    public void setStatValuePlayer1(String statValuePlayer1) {
        this.statValuePlayer1 = statValuePlayer1;
    }

    public String getStatValuePlayer2() {
        return statValuePlayer2;
    }

    public void setStatValuePlayer2(String statValuePlayer2) {
        this.statValuePlayer2 = statValuePlayer2;
    }
}
