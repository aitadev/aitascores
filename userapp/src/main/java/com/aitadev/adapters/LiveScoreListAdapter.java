package com.aitadev.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aitadev.sdk.aitamodels.PlayerScoreStatsModel;
import com.aitadev.sdk.aitamodels.StatsModel;
import com.aitadev.sdk.aitamodels.WrapperStatsModel;
import com.aitascores.shsadago.aitascores.R;

public class LiveScoreListAdapter extends RecyclerView.Adapter<LiveScoreViewHolder> {

    WrapperStatsModel wrapperStatsModel;

    private Context context;

    public LiveScoreListAdapter(Context context, WrapperStatsModel wrapperStatsModel) {
        this.context = context;
        this.wrapperStatsModel = wrapperStatsModel;
    }

    @NonNull
    @Override
    public LiveScoreViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //INFLATE A VIEW FROM XML
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.match_live_score_layout, null);

        //HOLDER
        LiveScoreViewHolder holder = new LiveScoreViewHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull LiveScoreViewHolder holder, int position) {

        if(wrapperStatsModel.getStatsModel()==null)
            return;
        PlayerScoreStatsModel player1 = wrapperStatsModel.getStatsModel().getPlayer1ScoreStatsModel();
        PlayerScoreStatsModel player2 = wrapperStatsModel.getStatsModel().getPlayer2ScoreStatsModel();

        //BIND DATA
        if(position == 0) {

            holder.player.setText(player1.getName() + ("player1".equalsIgnoreCase(wrapperStatsModel.getStatsModel().getServingPlayer()) ? "*" : ""));
            //holder.status.setText("player1".equalsIgnoreCase(statsModel.getServingPlayer()) ? "*" : "");
            holder.point.setText(getLivePoint(player1));
            holder.set1.setText(getSetScore(player1,0));
            holder.set2.setText(getSetScore(player1, 1));
            holder.set3.setText(getSetScore(player1, 2));
            holder.setScore.setText(getWonSets(player1));

        } else if (position == 1) {

            holder.player.setText(player2.getName() + ("player2".equalsIgnoreCase(wrapperStatsModel.getStatsModel().getServingPlayer()) ? "*" : ""));
            //holder.status.setText("player2".equalsIgnoreCase(statsModel.getServingPlayer()) ? "*" : "");
            holder.point.setText(getLivePoint(player2));
            holder.set1.setText(getSetScore(player2,0));
            holder.set2.setText(getSetScore(player2, 1));
            holder.set3.setText(getSetScore(player2, 2));
            holder.setScore.setText(getWonSets(player2));

        }


    }

    private boolean getServingPlayer(PlayerScoreStatsModel player) {

        return player.getName().equals(wrapperStatsModel.getStatsModel().getServingPlayer());
    }

    private String getWonSets(PlayerScoreStatsModel player1) {
        int numSets = player1.getSetList().items.size();
        int numWins = 0;
        for(int i=0;i<numSets;i++) {
            if("W".equals(player1.getSetList().items.get(i).getResult())) {
                numWins++;
            }
        }
        return String.valueOf(numWins);
    }

    private String getSetScore(PlayerScoreStatsModel player, int i) {
        if(i<player.getSetList().items.size() && player.getSetList().items.get(i) != null)
            return player.getSetList().items.get(i).getGameScore();
        else
            return "";
    }

    private String getLivePoint(PlayerScoreStatsModel player) {
        int currentSet = player.getSetList().items.size() - 1;
        return player.getSetList().items.get(currentSet).getPointScore();
    }



    @Override
    public int getItemCount() {
        return 2;
    }
}
