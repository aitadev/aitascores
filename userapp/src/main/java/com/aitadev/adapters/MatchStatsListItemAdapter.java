package com.aitadev.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aitadev.sdk.aitamodels.MatchStatModel;
import com.aitascores.shsadago.aitascores.R;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class MatchStatsListItemAdapter extends RecyclerView.Adapter<MatchStatsListItemViewHolder> {

    MatchStatModel matchStatModel;
    private Context context;
    private List<StatNameValues> statNameValues = new ArrayList<>();


    public MatchStatsListItemAdapter(Context context, MatchStatModel matchStatModel) {
        this.context = context;
        this.matchStatModel = matchStatModel;
        ObjectToArrayAdapter();
    }

    private void ObjectToArrayAdapter() {
        Field[] matchStatFields = this.matchStatModel.getClass().getDeclaredFields();
        statNameValues.clear();
        for (Field field: matchStatFields) {
            field.setAccessible(true);
            String name = field.getName();
            if(name.equals("serialVersionUID")){

            } else {
                try {
                    String value = (String) field.get(this.matchStatModel);
                    if (value!=null)
                        statNameValues.add(new StatNameValues(name,value));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

        }
    }


    @NonNull
    @Override
    public MatchStatsListItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //INFLATE A VIEW FROM XML
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.match_stats_item_layout, null);

        //HOLDER
        MatchStatsListItemViewHolder holder = new MatchStatsListItemViewHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MatchStatsListItemViewHolder holder, int position) {

       //BIND DATA
        holder.statName.setText(statNameValues.get(position).getStatName());
        holder.statValue.setText(statNameValues.get(position).getStatValuePlayer1());
    }

    @Override
    public int getItemCount() {
        return statNameValues.size();
    }
}
