package com.aitadev.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.aitascores.shsadago.aitascores.R;


public class AdContentViewHolder extends RecyclerView.ViewHolder {

    public TextView adField,adValue;
    public AdContentViewHolder(@NonNull View itemView) {
        super(itemView);
        this.adField = (TextView)itemView.findViewById(R.id.ad_field);
        this.adValue = (TextView)itemView.findViewById(R.id.ad_value);
    }
}
