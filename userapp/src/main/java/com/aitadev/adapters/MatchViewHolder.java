package com.aitadev.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.aitadev.aitascores.aitaUx.ItemClickListener;
import com.aitascores.shsadago.aitascores.R;

public class MatchViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView set1,game1,point1,player1,status1;
    public TextView set2,game2,point2,player2,status2;

    private ItemClickListener itemClickListener;


    public MatchViewHolder(View itemView) {
        super(itemView);
        this.player1 = (TextView) itemView.findViewById(R.id.player1);
        this.status1 = (TextView) itemView.findViewById(R.id.status1);
        this.set1 = (TextView) itemView.findViewById(R.id.set1);
        this.game1 = (TextView) itemView.findViewById(R.id.game1);
        this.point1 = (TextView) itemView.findViewById(R.id.point1);

        this.player2 = (TextView) itemView.findViewById(R.id.player2);
        this.status2 = (TextView) itemView.findViewById(R.id.status2);
        this.set2 = (TextView) itemView.findViewById(R.id.set2);
        this.game2 = (TextView) itemView.findViewById(R.id.game2);
        this.point2 = (TextView) itemView.findViewById(R.id.point2);

        itemView.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        this.itemClickListener.onItemClick(v, getLayoutPosition());
    }

    public void setItemClickListener(ItemClickListener ic) {
        this.itemClickListener = ic;

    }

}
