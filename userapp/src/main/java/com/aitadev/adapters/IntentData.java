package com.aitadev.adapters;

public class IntentData {
    private String tid;
    private String aid;
    private String tname;

    public IntentData(String tid, String tname){
        this.tid = tid;
        this.tname = tname;
    }

    public IntentData(String tid, String tname, String aid){
        this.tid = tid;
        this.tname = tname;
        this.aid = aid;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }
}
