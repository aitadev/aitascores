package com.aitadev.adapters;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.aitadev.aitascores.aitaUx.ItemClickListener;
import com.aitascores.shsadago.aitascores.R;

import org.w3c.dom.Text;

public class MatchStatsViewHolder extends RecyclerView.ViewHolder {

    public TextView statPlayer1;
    public TextView statPlayer2;
    public TextView statName;


    public MatchStatsViewHolder(View itemView, Context context) {
        super(itemView);
        this.statPlayer1 = itemView.findViewById(R.id.stat_player1);
        this.statPlayer2 = itemView.findViewById(R.id.stat_player2);
        this.statName = itemView.findViewById(R.id.stat_name);
    }


}
