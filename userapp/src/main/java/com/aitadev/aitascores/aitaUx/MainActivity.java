package com.aitadev.aitascores.aitaUx;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aitadev.adapters.TournamentListAdapter;
import com.aitadev.sdk.APINode;
import com.aitadev.sdk.ConnectionManager;
import com.aitadev.sdk.ConnectionTask;
import com.aitascores.shsadago.aitascores.R;

import org.json.JSONException;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private String[] names = {"AITA Under 12", "AITA under 14", "AITA under 16", "AITA MEN", "AITA women"};
    private Map<String,String> patternValue = new HashMap<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tournament);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //drawTournamentList();

        TextView tv = (TextView)findViewById(R.id.toolbar_title);
        tv.setText("AITASCORES");

        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {


            case R.id.action_ad:
                // User chose the "Favorite" action, mark the current item
                // as a favorite...
                return super.onOptionsItemSelected(item);

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        drawTournamentList();

    }

    private void drawTournamentList() {
        RecyclerView rv = (RecyclerView) findViewById(R.id.tournamentListView);



        //SET PROPERTIES
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setItemAnimator(new DefaultItemAnimator());

        //ADAPTER
        ConnectionManager.getSharedInstance().doGet(ConnectionTask.obtainGetTask(APINode.TOURNAMENT_LIST, this, rv, patternValue), new ConnectionManager.ConnectionTaskListenerAdapter() {
            @Override
            public void onConnectionComplete(ConnectionTask task, String json) {
                try {
                    super.onConnectionComplete(task, json);
                } catch (JSONException e) {
                    e.printStackTrace();

                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
                TournamentListAdapter adapter = new TournamentListAdapter(task.context, task.response);
                ((RecyclerView) task.view).setAdapter(adapter);
            }
        });
    }



}
