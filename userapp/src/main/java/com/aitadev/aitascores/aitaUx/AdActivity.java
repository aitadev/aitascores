package com.aitadev.aitascores.aitaUx;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.aitadev.adapters.AdContentAdapter;
import com.aitadev.adapters.TournamentListAdapter;
import com.aitadev.sdk.APINode;
import com.aitadev.sdk.ConnectionManager;
import com.aitadev.sdk.ConnectionTask;
import com.aitascores.shsadago.aitascores.R;

import org.json.JSONException;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class AdActivity extends AppCompatActivity {

    private static final String AID = "{aid}";

    private String aid;
    private String tname;

    private Map<String,String> patternValue = new HashMap<>();
    Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent i = getIntent();
        aid = i.getExtras().getString("Aid");
        tname = i.getExtras().getString("Tname");

        TextView adText = (TextView) findViewById(R.id.ad_text);
        adText.setText(tname);

        TextView tv = (TextView)findViewById(R.id.toolbar_title);
        tv.setText("");

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.context = this;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {


            case R.id.action_ad:

                drawAdLayout();
                return true;

            default:

                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        drawAdLayout();
    }


    private void drawAdLayout() {
        RecyclerView rv = (RecyclerView) findViewById(R.id.ad_table);



        //SET PROPERTIES
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setItemAnimator(new DefaultItemAnimator());

        patternValue.put(AID, aid);
        //ADAPTER
        ConnectionManager.getSharedInstance().doGet(ConnectionTask.obtainGetTask(APINode.AD_DETAILS, this, rv, patternValue), new ConnectionManager.ConnectionTaskListenerAdapter() {
            @Override
            public void onConnectionComplete(ConnectionTask task, String json) {
                try {
                    super.onConnectionComplete(task, json);
                } catch (JSONException e) {
                    e.printStackTrace();

                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
                AdContentAdapter adapter = new AdContentAdapter(task.context, task.response);
                ((RecyclerView) task.view).setAdapter(adapter);
            }
        });
    }

}
