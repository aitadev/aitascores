package com.aitadev.aitascores.aitaUx;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aitadev.adapters.IntentData;
import com.aitadev.adapters.MatchListAdapter;
import com.aitadev.sdk.APINode;
import com.aitadev.sdk.ConnectionManager;
import com.aitadev.sdk.ConnectionTask;
import com.aitascores.shsadago.aitascores.R;

import org.json.JSONException;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;


public class MatchActivity extends AppCompatActivity {

    private static final String TID = "{tid}";
    private static final String STATUS = "{status}";
    private String tId, name, aId;
    private Map<String,String> patternValue = new HashMap<>();
    TextView tv;
    private Context context;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match);
        this.context = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent i = getIntent();
        tId = i.getExtras().getString("Tid");
        aId = i.getExtras().getString("Aid");
        if (aId == null)
            aId = "0";
        name = i.getExtras().getString("Tname");


        TextView tv = (TextView)findViewById(R.id.toolbar_title);
        tv.setText("List of Matches");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RelativeLayout adClick =(RelativeLayout)findViewById(R.id.ad_layout);
        adClick.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(context, AdActivity.class);

                //ADD DATA TO OUR INTENT
                i.putExtra("Aid", aId);
                i.putExtra("Tname", name);


                //START DETAIL ACTIVITY
                context.startActivity(i);
            }
        });

        //drawMatches();
    }
    @SuppressLint("RestrictedApi")
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        if(menu instanceof MenuBuilder){
            MenuBuilder m = (MenuBuilder) menu;
            m.setOptionalIconsVisible(true);
        }
        return true;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {


            case R.id.action_ad:
                // User chose the "Favorite" action, mark the current item
                // as a favorite...
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        drawMatches();
    }

    private void drawMatches() {
        tv = (TextView) findViewById(R.id.tournamementName);
        tv.setText(name);

        RecyclerView liveList = (RecyclerView) findViewById(R.id.live_match_score_list);
        liveList.setLayoutManager(new LinearLayoutManager(this));
        liveList.setItemAnimator(new DefaultItemAnimator());
        liveList.setNestedScrollingEnabled(false);

        RecyclerView scheduledList = (RecyclerView) findViewById(R.id.scheduled_match_status_list);
        scheduledList.setLayoutManager(new LinearLayoutManager(this));
        scheduledList.setItemAnimator(new DefaultItemAnimator());
        scheduledList.setNestedScrollingEnabled(false);

        RecyclerView completedList = (RecyclerView) findViewById(R.id.completed_match_status_list);
        completedList.setLayoutManager(new LinearLayoutManager(this));
        completedList.setItemAnimator(new DefaultItemAnimator());
        completedList.setNestedScrollingEnabled(false);

        //ADAPTER
        patternValue.put(TID, tId);
        patternValue.put(STATUS,"InProgress");
        Log.d("***sheera***", "Going to fetch inprogress match list");
        ConnectionManager.getSharedInstance().doGet(ConnectionTask.obtainGetTask(APINode.MATCH_LIST, this, liveList, patternValue), new ConnectionManager.ConnectionTaskListenerAdapter() {
            @Override
            public void onConnectionComplete(ConnectionTask task, String json) {
                try {
                    Log.d("***sheera***", "Fetched inprogress match list");

                    super.onConnectionComplete(task, json);
                    if(task.response.items.size() > 0) {
                        findViewById(R.id.live_match_status).setVisibility(View.VISIBLE);
                        MatchListAdapter adapter = new MatchListAdapter(task.context, task.response, new IntentData(tId, name, aId));
                        ((RecyclerView) task.view).setAdapter(adapter);
                    } else {
                        findViewById(R.id.live_match_status).setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }

            }
        });

        patternValue.put(STATUS,"scheduled");
        Log.d("***sheera***", "Going to fetch scheduled match list");

        ConnectionManager.getSharedInstance().doGet(ConnectionTask.obtainGetTask(APINode.MATCH_LIST, this, scheduledList, patternValue), new ConnectionManager.ConnectionTaskListenerAdapter() {
            @Override
            public void onConnectionComplete(ConnectionTask task, String json) {
                try {
                    Log.d("***sheera***", "Fetched scheduled match list");
                    super.onConnectionComplete(task, json);
                    findViewById(R.id.scheduled_match_status).setVisibility(View.VISIBLE);
                    if(task.response.items.size() > 0) {
                        MatchListAdapter adapter = new MatchListAdapter(task.context, task.response, new IntentData(tId, name, aId));
                        ((RecyclerView) task.view).setAdapter(adapter);
                    } else {
                        findViewById(R.id.scheduled_match_status).setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }

            }
        });

        patternValue.put(STATUS,"Completed");
        Log.d("***sheera***", "Going to fetch completed match list");
        ConnectionManager.getSharedInstance().doGet(ConnectionTask.obtainGetTask(APINode.MATCH_LIST, this, completedList, patternValue), new ConnectionManager.ConnectionTaskListenerAdapter() {
            @Override
            public void onConnectionComplete(ConnectionTask task, String json) {
                try {
                    Log.d("***sheera***", "Fetched completed match list");
                    super.onConnectionComplete(task, json);
                    if(task.response.items.size() > 0) {
                        findViewById(R.id.completed_match_status).setVisibility(View.VISIBLE);
                        MatchListAdapter adapter = new MatchListAdapter(task.context, task.response, new IntentData(tId, name, aId));
                        ((RecyclerView) task.view).setAdapter(adapter);
                    } else {
                        findViewById(R.id.completed_match_status).setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }

            }
        });
    }

}


