package com.aitadev.aitascores.aitaUx;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aitadev.adapters.LiveScoreListAdapter;
import com.aitadev.adapters.MatchStatsListAdapter;
import com.aitadev.sdk.APINode;
import com.aitadev.sdk.ConnectionManager;
import com.aitadev.sdk.ConnectionTask;
import com.aitadev.sdk.aitamodels.StatsModel;
import com.aitadev.sdk.aitamodels.WrapperStatsModel;
import com.aitascores.shsadago.aitascores.R;

import org.json.JSONException;
import org.w3c.dom.Text;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;


public class StatsActivity extends AppCompatActivity {

    private static final String TID = "{tid}";
    private static final String MID = "{mid}";

    private Map<String,String> patternValue = new HashMap<>();
    WrapperStatsModel wrapperStatsModel;
    LiveScoreListAdapter liveScoreListAdapter;
    MatchStatsListAdapter matchStatsListAdapter;
    Context context;
    private String aId;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_data_stats);
        this.context = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Intent i = getIntent();
        final String tId = i.getExtras().getString("Tid");
        final String mId = i.getExtras().getString("Mid");
        final String name = i.getExtras().getString("TName");
        aId = i.getExtras().getString("Aid");
        if (aId == null)
            aId = "0";


        RelativeLayout adClick =(RelativeLayout)findViewById(R.id.ad_layout);
        adClick.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(context, AdActivity.class);

                //ADD DATA TO OUR INTENT
                i.putExtra("Aid", aId);
                i.putExtra("Tname", name);


                //START DETAIL ACTIVITY
                context.startActivity(i);
            }
        });

        TextView tv = (TextView)findViewById(R.id.toolbar_title);
        tv.setText("Match Stats");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        drawMatches(tId, mId, name);
    }
    @SuppressLint("RestrictedApi")
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        if(menu instanceof MenuBuilder){
            MenuBuilder m = (MenuBuilder) menu;
            m.setOptionalIconsVisible(true);
        }
        return true;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {


            case R.id.action_ad:
                // User chose the "Favorite" action, mark the current item
                // as a favorite...

                fetchStats();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
    @Override
    protected void onResume() {
        super.onResume();
    }


    private void fetchStats() {
        ConnectionManager.getSharedInstance().doGet(ConnectionTask.obtainGetTask(APINode.LIVE_SCORE_STATS, this, null, patternValue), new ConnectionManager.ConnectionTaskListenerAdapter() {
            @Override
            public void onConnectionComplete(ConnectionTask task, String json) {
                try {
                    super.onConnectionComplete(task, json);
                    wrapperStatsModel.setStatsModel((StatsModel)task.response.items.get(0));
                    ((TextView)findViewById(R.id.stat_player1_name)).setText(wrapperStatsModel.getStatsModel().getPlayer1ScoreStatsModel().getName());
                    ((TextView)findViewById(R.id.stat_player2_name)).setText(wrapperStatsModel.getStatsModel().getPlayer2ScoreStatsModel().getName());
                    ((TextView)findViewById(R.id.stat_name)).setText("Stats");
                    liveScoreListAdapter.notifyDataSetChanged();
                    matchStatsListAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();

                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }





            }
        });
    }

    private void drawMatches(String tId, String mId, final String name) {

        //ADAPTER
        patternValue.put(TID, tId);
        patternValue.put(MID, mId);

        wrapperStatsModel = new WrapperStatsModel();

        ((TextView)findViewById(R.id.tournamementName)).setText(name);
        final RecyclerView liveMatchScoreList = (RecyclerView) findViewById(R.id.live_match_score_list);
        liveMatchScoreList.setLayoutManager(new LinearLayoutManager(this));
        liveMatchScoreList.setItemAnimator(new DefaultItemAnimator());


        RecyclerView liveMatchStatsList = findViewById(R.id.stat_table);
        liveMatchStatsList.setLayoutManager(new GridLayoutManager(this,1));
        liveMatchStatsList.setItemAnimator(new DefaultItemAnimator());


        //Button refreshButton = findViewById(R.id.matchRound);

        liveScoreListAdapter = new LiveScoreListAdapter(context, wrapperStatsModel);
        matchStatsListAdapter = new MatchStatsListAdapter(context, wrapperStatsModel);

        ((RecyclerView) findViewById(R.id.live_match_score_list)).setAdapter(liveScoreListAdapter);
        ((RecyclerView) findViewById(R.id.stat_table)).setAdapter(matchStatsListAdapter);

        fetchStats();

//        refreshButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                fetchStats();
//            }
//
//        });



    }

}


