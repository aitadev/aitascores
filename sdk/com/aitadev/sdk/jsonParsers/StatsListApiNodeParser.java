package com.aitadev.sdk.jsonParsers;

import com.aitadev.sdk.aitamodels.ListModel;
import com.aitadev.sdk.aitamodels.MatchStatModel;
import com.aitadev.sdk.aitamodels.PlayerScoreStatsModel;
import com.aitadev.sdk.aitamodels.SetModel;
import com.aitadev.sdk.aitamodels.StatsModel;
import com.aitadev.sdk.api.IApiNodeParser;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonStreamContext;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class StatsListApiNodeParser extends AbstractApiNodeParser implements IApiNodeParser {

    @Override
    protected void parseItems(JsonParser jsonParser, JsonStreamContext parent, ListModel listModel, String[] parseKeys) throws IOException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {

        JsonToken jsonToken = jsonParser.currentToken();

        if(jsonToken == JsonToken.START_ARRAY) {

            while (jsonParser.nextToken() == JsonToken.START_OBJECT) {
                SetModel setModel = new SetModel();
                parseModel(jsonParser, jsonParser.getParsingContext().getParent(), setModel, parseKeys);
                listModel.items.add(setModel);
            }
        }

    }

    @Override
    protected ListModel getListModel() {
        ListModel<StatsModel> listModel = new ListModel<>();
        return listModel;
    }

    @Override
    protected void parseModelSpecificJson(JsonParser jsonParser, JsonStreamContext parent, String[] parseKeys, Object model) throws IOException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        JsonToken jsonToken = jsonParser.getCurrentToken();
        if ((jsonToken == JsonToken.END_OBJECT) && jsonParser.getParsingContext().equals(parent)) {
            return;
        }
        String name = jsonParser.getCurrentName();
        JsonToken nextToken = jsonParser.nextToken();
        if (jsonToken == JsonToken.FIELD_NAME && nextToken == JsonToken.START_OBJECT) {
            if ("player1".equals(name)) {
                PlayerScoreStatsModel playerScoreStatsModel = new PlayerScoreStatsModel();
                parseModel(jsonParser, jsonParser.getParsingContext().getParent(), playerScoreStatsModel, parseKeys);
                ((StatsModel) model).setPlayer1ScoreStatsModel(playerScoreStatsModel);
            } else if ("player2".equals(name)) {
                PlayerScoreStatsModel playerScoreStatsModel = new PlayerScoreStatsModel();
                parseModel(jsonParser, jsonParser.getParsingContext().getParent(), playerScoreStatsModel, parseKeys);
                ((StatsModel) model).setPlayer2ScoreStatsModel(playerScoreStatsModel);
            } else if ("stats".equals(name)) {
                MatchStatModel matchStatModel = new MatchStatModel();
                parseModel(jsonParser, jsonParser.getParsingContext().getParent(), matchStatModel, parseKeys);
                ((PlayerScoreStatsModel) model).setMatchStatModel(matchStatModel);
            }
        }
        if (jsonToken == JsonToken.FIELD_NAME && nextToken == JsonToken.START_ARRAY) {
            if ("sets".equals(name)) {
                ListModel<SetModel> listModel = new ListModel<>();
                parseItems(jsonParser, jsonParser.getParsingContext().getParent(), listModel, parseKeys);
                ((PlayerScoreStatsModel) model).setSetList(listModel);
            }
        }
    }

    @Override
    public ListModel parse(JsonParser jsonParser, String[] parseKeys) throws IOException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        StatsModel model = new StatsModel();
        jsonParser.nextToken();
        parseModel(jsonParser, jsonParser.getParsingContext().getParent(), model, parseKeys);
        ListModel<StatsModel> listModel = getListModel();
        listModel.items.add(model);
        return listModel;
    }
}
