package com.aitadev.sdk.jsonParsers;

import com.aitadev.sdk.aitamodels.ListModel;
import com.aitadev.sdk.api.IApiNodeParser;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonStreamContext;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

public abstract class AbstractApiNodeParser implements IApiNodeParser {

    protected ListModel listModel = getListModel();

    protected String capitalizeFirstLetter(String original) {
        if (original == null || original.length() == 0) {
            return original;
        }
        return original.substring(0, 1).toUpperCase() + original.substring(1);
    }

    protected AbstractApiNodeParser(){}

    protected void parseJSON(JsonParser jsonParser, JsonStreamContext parent, String[] parseKeys, Object model) throws IOException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {

        while (true) {
            JsonToken jsonToken = jsonParser.getCurrentToken();
            if ((jsonToken == JsonToken.END_OBJECT) && jsonParser.getParsingContext().equals(parent)) {
                break;
            }
            String name = jsonParser.getCurrentName();
            if (jsonToken == JsonToken.FIELD_NAME) {
                if (Arrays.asList(parseKeys).contains(name)) {
                    if(model!=null)
                        model.getClass().getMethod("set"+capitalizeFirstLetter(name),jsonParser.getText().getClass()).invoke(model,jsonParser.nextTextValue());
                } else if("items".equals(name)) {
                    parseItems(jsonParser, jsonParser.getParsingContext().getParent(), listModel, parseKeys);
                } else {
                    parseModelSpecificJson(jsonParser, jsonParser.getParsingContext().getParent(), parseKeys, model);
                }
            }
            jsonParser.nextToken();
        }
    }

    protected void parseModel(JsonParser jsonParser, JsonStreamContext parent, Object model, String[] parseKeys) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException, IOException {
        JsonToken jsonToken = jsonParser.nextToken();
        if ((jsonToken == JsonToken.END_OBJECT) && jsonParser.getParsingContext().equals(parent)) {
            return;
        }

        if (jsonToken == JsonToken.FIELD_NAME) {
            parseJSON(jsonParser, jsonParser.getParsingContext().getParent(), parseKeys, model);
        }

    }

    private ListModel fillListModel(JsonParser jsonParser, String[] parseKeys) throws IOException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        while (true) {
            JsonToken currentJsonToken  = jsonParser.getCurrentToken();
            JsonToken jsonToken = jsonParser.nextToken();

            if (currentJsonToken == JsonToken.END_OBJECT || jsonToken == JsonToken.END_OBJECT || currentJsonToken == jsonToken.END_ARRAY) {
                break;
            }

            if ((jsonToken == null) || (jsonToken == JsonToken.NOT_AVAILABLE)) {
                throw new JsonParseException("bad JSON", jsonParser.getCurrentLocation());
            }


            if(jsonToken == JsonToken.START_ARRAY) {
                parseItems(jsonParser, jsonParser.getParsingContext().getParent(), listModel, parseKeys);
            } else if(jsonToken == JsonToken.FIELD_NAME) {
                final  String name = jsonParser.getCurrentName();
                if("items".equals(name) || "draw".equals(name)) {
                    jsonParser.nextToken();
                    parseItems(jsonParser, jsonParser.getParsingContext().getParent(), listModel, parseKeys);
                } else {
                    jsonParser.nextToken();
                    parseModel(jsonParser, jsonParser.getParsingContext().getParent(), null, parseKeys);
                }
            }

        }
        return listModel;
    }


    protected abstract ListModel getListModel();

    protected abstract void parseItems(JsonParser jsonParser, JsonStreamContext parent, ListModel listModel, String[] parseKeys) throws IOException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;

    protected abstract  void parseModelSpecificJson(JsonParser jsonParser, JsonStreamContext parent, String[] parseKeys, Object model) throws IOException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;

    @Override
    public ListModel parse(JsonParser jsonParser, String[] parseKeys) throws IOException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        return fillListModel(jsonParser, parseKeys);
    }
}
