package com.aitadev.sdk.jsonParsers;

import com.aitadev.sdk.aitamodels.ListModel;
import com.aitadev.sdk.aitamodels.MatchModel;
import com.aitadev.sdk.aitamodels.PlayerScoreModel;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonStreamContext;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class MatchListApiNodeParser extends AbstractApiNodeParser {

    public MatchListApiNodeParser(){}

    @Override
    protected void parseItems(JsonParser jsonParser, JsonStreamContext parent, ListModel listModel, String[] parseKeys) throws IOException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {

    }

    private void parseDraw(JsonParser jsonParser, JsonStreamContext parent, String[] parseKeys) throws IOException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {

        JsonToken jsonToken = jsonParser.currentToken();

        if(jsonToken == JsonToken.START_ARRAY) {

            while (jsonParser.nextToken() == JsonToken.START_OBJECT) {

                parseModel(jsonParser, jsonParser.getParsingContext().getParent(), null, parseKeys);

            }
        }

    }

    private void parseMatches(JsonParser jsonParser, JsonStreamContext parent, String[] parseKeys) throws IOException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {

        JsonToken jsonToken = jsonParser.currentToken();

        if(jsonToken == JsonToken.START_ARRAY) {

            while (jsonParser.nextToken() == JsonToken.START_OBJECT) {
                MatchModel matchModel = new MatchModel();
                parseModel(jsonParser, jsonParser.getParsingContext().getParent(), matchModel, parseKeys);
                listModel.items.add(matchModel);
            }
        }

    }

    @Override
    protected ListModel getListModel() {
        ListModel<MatchModel> listModel = new ListModel<>();
        return listModel;
    }

    @Override
    protected void parseModelSpecificJson(JsonParser jsonParser, JsonStreamContext parent, String[] parseKeys, Object model) throws IOException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {

        JsonToken jsonToken = jsonParser.getCurrentToken();
        JsonToken nextJsonToken = jsonParser.nextToken();

        if ((jsonToken == JsonToken.END_OBJECT) && jsonParser.getParsingContext().equals(parent)) {
            return;
        }
        String name = jsonParser.getCurrentName();

        if (jsonToken == JsonToken.FIELD_NAME && nextJsonToken == JsonToken.START_OBJECT) {
            if ("player1".equals(name)) {
                PlayerScoreModel playerScoreModel = new PlayerScoreModel();
                parseModel(jsonParser, jsonParser.getParsingContext().getParent(), playerScoreModel, parseKeys);
                ((MatchModel) model).setPlayer1ScoreModel(playerScoreModel);
            } else if ("player2".equals(name)) {
                PlayerScoreModel playerScoreModel = new PlayerScoreModel();
                parseModel(jsonParser, jsonParser.getParsingContext().getParent(), playerScoreModel, parseKeys);
                ((MatchModel) model).setPlayer2ScoreModel(playerScoreModel);
            } else {
               parseModel(jsonParser, jsonParser.getParsingContext().getParent(), model, parseKeys);
            }
        }

        if (jsonToken == JsonToken.FIELD_NAME && nextJsonToken == JsonToken.START_ARRAY) {
            if("draw".equals(name)) {
                parseDraw(jsonParser, jsonParser.getParsingContext().getParent(), parseKeys);
            }
            if("matches".equals(name)) {
                parseMatches(jsonParser, jsonParser.getParsingContext().getParent(), parseKeys);

            }
        }
    }
}
