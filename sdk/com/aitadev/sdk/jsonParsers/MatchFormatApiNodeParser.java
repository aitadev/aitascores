package com.aitadev.sdk.jsonParsers;

import com.aitadev.sdk.aitamodels.ListModel;
import com.aitadev.sdk.aitamodels.MatchFormat;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonStreamContext;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class MatchFormatApiNodeParser extends AbstractApiNodeParser{

    @Override
    protected ListModel getListModel() {
        ListModel<MatchFormat> listModel = new ListModel<>();
        return listModel;
    }

    @Override
    protected void parseItems(JsonParser jsonParser, JsonStreamContext parent, ListModel listModel, String[] parseKeys) throws IOException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {

    }

    @Override
    protected void parseModelSpecificJson(JsonParser jsonParser, JsonStreamContext parent, String[] parseKeys, Object model) throws IOException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {

    }
    @Override
    public ListModel parse(JsonParser jsonParser, String[] parseKeys) throws IOException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        MatchFormat model = new MatchFormat();
        jsonParser.nextToken();
        parseModel(jsonParser, jsonParser.getParsingContext().getParent(), model, parseKeys);
        ListModel<MatchFormat> listModel = getListModel();
        listModel.items.add(model);
        return listModel;
    }

}
