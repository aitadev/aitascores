package com.aitadev.sdk.jsonParsers;

import com.aitadev.sdk.aitamodels.AdModel;
import com.aitadev.sdk.aitamodels.AdModelJsonFormat;
import com.aitadev.sdk.aitamodels.AdModelUI;
import com.aitadev.sdk.aitamodels.ListModel;
import com.aitadev.sdk.aitamodels.MatchFormat;
import com.aitadev.sdk.aitamodels.TournamentModel;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonStreamContext;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AdDetailsApiNodeParser extends AbstractApiNodeParser {

    public AdDetailsApiNodeParser(){}

    private AdModelJsonFormat adModelJsonFormat = new AdModelJsonFormat();

    protected void parseItems(JsonParser jsonParser, JsonStreamContext parent, ListModel listModel, String[] parseKeys) throws IOException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {

        JsonToken jsonToken = jsonParser.nextToken();

        if(jsonToken == JsonToken.START_ARRAY) {

            while (jsonParser.nextToken() == JsonToken.START_OBJECT) {
                AdModel adModel = new AdModel();
                parseModel(jsonParser, jsonParser.getParsingContext().getParent(), adModel, parseKeys);
                if(adModelJsonFormat.getAdList() == null) {
                    List<AdModel> lm = new ArrayList<>();
                    adModelJsonFormat.setAdList(lm);
                }
                adModelJsonFormat.getAdList().add(adModel);
            }
        }

    }

    @Override
    protected ListModel getListModel() {
        ListModel<AdModel> listModel = new ListModel<>();
        return listModel;
    }

    private ListModel getListModelUI() {
        ListModel<AdModelUI> listModel = new ListModel<>();
        return listModel;
    }


    @Override
    protected void parseModelSpecificJson(JsonParser jsonParser, JsonStreamContext parent, String[] parseKeys, Object model) {
    }

    @Override
    protected void parseJSON(JsonParser jsonParser, JsonStreamContext parent, String[] parseKeys, Object model) throws IOException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {

        while (true) {
            JsonToken jsonToken = jsonParser.getCurrentToken();
            if ((jsonToken == JsonToken.END_OBJECT) && jsonParser.getParsingContext().equals(parent)) {
                break;
            }
            String name = jsonParser.getCurrentName();
            if (jsonToken == JsonToken.FIELD_NAME) {
                if (Arrays.asList(parseKeys).contains(name)) {
                    if(model!=null)
                        model.getClass().getMethod("set"+capitalizeFirstLetter(name),jsonParser.getText().getClass()).invoke(model,jsonParser.nextTextValue());
                } else if("items".equals(name)) {
                    parseItems(jsonParser, jsonParser.getParsingContext().getParent(), null,  parseKeys);
                } else {
                    parseModelSpecificJson(jsonParser, jsonParser.getParsingContext().getParent(), parseKeys, model);
                }
            }
            jsonParser.nextToken();
        }
    }

    @Override
    protected void parseModel(JsonParser jsonParser, JsonStreamContext parent, Object model, String[] parseKeys) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException, IOException {
        JsonToken jsonToken = jsonParser.nextToken();
        if ((jsonToken == JsonToken.END_OBJECT) && jsonParser.getParsingContext().equals(parent)) {
            return;
        }

        if (jsonToken == JsonToken.FIELD_NAME) {
            parseJSON(jsonParser, jsonParser.getParsingContext().getParent(), parseKeys, model);
        }

    }

    @Override
    public ListModel parse(JsonParser jsonParser, String[] parseKeys) throws IOException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        AdModelJsonFormat model = adModelJsonFormat;
        jsonParser.nextToken();
        parseModel(jsonParser, jsonParser.getParsingContext().getParent(), model, parseKeys);
        ListModel<AdModelUI> listModel = getListModelUI();
        for(int i=0; i < model.getAdList().size(); i++) {
            AdModelUI ad = new AdModelUI();
            ad.setAid(model.getAid());
            ad.setFeeStructure(model.getAdList().get(i).getFeeStructure());
            ad.setHeadCoach(model.getAdList().get(i).getHeadCoach());
            ad.setHeadCoachContact(model.getAdList().get(i).getHeadCoachContact());
            ad.setIsFitnessAvailable(model.getAdList().get(i).getIsFitnessAvailable());
            ad.setLocation(model.getAdList().get(i).getLocation());
            ad.setNumAitaPlayers(model.getAdList().get(i).getNumAitaPlayers());
            ad.setNumClayCourts(model.getAdList().get(i).getNumClayCourts());
            ad.setNumCoaches(model.getAdList().get(i).getNumCoaches());
            ad.setNumFloodLights(model.getAdList().get(i).getNumFloodLights());
            ad.setNumHardCourts(model.getAdList().get(i).getNumHardCourts());
            listModel.items.add(ad);
        }
        return listModel;
    }

}
