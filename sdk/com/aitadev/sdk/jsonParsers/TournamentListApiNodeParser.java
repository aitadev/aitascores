package com.aitadev.sdk.jsonParsers;

import com.aitadev.sdk.aitamodels.ListModel;
import com.aitadev.sdk.aitamodels.TournamentModel;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonStreamContext;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class TournamentListApiNodeParser extends AbstractApiNodeParser {

    public  TournamentListApiNodeParser(){}

    @Override
    protected void parseItems(JsonParser jsonParser, JsonStreamContext parent, ListModel listModel, String[] parseKeys) throws IOException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {

        JsonToken jsonToken = jsonParser.currentToken();

        if(jsonToken == JsonToken.START_ARRAY) {

            while (jsonParser.nextToken() == JsonToken.START_OBJECT) {
                TournamentModel tournamentModel = new TournamentModel();
                parseModel(jsonParser, jsonParser.getParsingContext().getParent(), tournamentModel, parseKeys);
                listModel.items.add(tournamentModel);
            }
        }

    }

    @Override
    protected ListModel getListModel() {
        ListModel<TournamentModel> listModel = new ListModel<>();
        return listModel;
    }

    @Override
    protected void parseModelSpecificJson(JsonParser jsonParser, JsonStreamContext parent, String[] parseKeys, Object model) {


    }

}
