package com.aitadev.sdk.api;

import com.aitadev.sdk.ConnectionTask;

import org.json.JSONException;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public interface IConnectionTaskListener {
    boolean onConnectionStart(ConnectionTask task);

    void onConnectionResponse(ConnectionTask task, Map<String, String> headers, int status);

    void onConnectionFailed(ConnectionTask task, IOException exception);

    void onConnectionCanceled(ConnectionTask task);

    void onConnectionComplete(ConnectionTask task, String json) throws JSONException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException, IOException;
}
