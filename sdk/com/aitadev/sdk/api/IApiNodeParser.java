package com.aitadev.sdk.api;

import com.aitadev.sdk.aitamodels.ListModel;
import com.fasterxml.jackson.core.JsonParser;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public interface IApiNodeParser {

    ListModel parse(JsonParser jsonParser, String[] parseKeys) throws IOException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;

}

