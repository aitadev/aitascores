package com.aitadev.sdk.aitamodels;

import java.io.Serializable;

public class AdModel implements Serializable {

    private String location;
    private String numClayCourts;
    private String numHardCourts;
    private String numFloodLights;
    private String numCoaches;
    private String numMarkers;
    private String numAitaPlayers;
    private String headCoach;
    private String headCoachContact;
    private String isFitnessAvailable;
    private String feeStructure;

    public AdModel(){}

    public String getNumMarkers() {
        return numMarkers;
    }

    public void setNumMarkers(String numMarkers) {
        this.numMarkers = numMarkers;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getNumClayCourts() {
        return numClayCourts;
    }

    public void setNumClayCourts(String numClayCourts) {
        this.numClayCourts = numClayCourts;
    }

    public String getNumHardCourts() {
        return numHardCourts;
    }

    public void setNumHardCourts(String numHardCourts) {
        this.numHardCourts = numHardCourts;
    }

    public String getNumFloodLights() {
        return numFloodLights;
    }

    public void setNumFloodLights(String numFloodLights) {
        this.numFloodLights = numFloodLights;
    }

    public String getNumCoaches() {
        return numCoaches;
    }

    public void setNumCoaches(String numCoaches) {
        this.numCoaches = numCoaches;
    }

    public String getNumAitaPlayers() {
        return numAitaPlayers;
    }

    public void setNumAitaPlayers(String numAitaPlayers) {
        this.numAitaPlayers = numAitaPlayers;
    }

    public String getHeadCoach() {
        return headCoach;
    }

    public void setHeadCoach(String headCoach) {
        this.headCoach = headCoach;
    }

    public String getHeadCoachContact() {
        return headCoachContact;
    }

    public void setHeadCoachContact(String headCoachContact) {
        this.headCoachContact = headCoachContact;
    }

    public String getIsFitnessAvailable() {
        return isFitnessAvailable;
    }

    public void setIsFitnessAvailable(String isFitnessAvailable) {
        this.isFitnessAvailable = isFitnessAvailable;
    }

    public String getFeeStructure() {
        return feeStructure;
    }

    public void setFeeStructure(String feeStructure) {
        this.feeStructure = feeStructure;
    }
}
