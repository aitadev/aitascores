package com.aitadev.sdk.aitamodels;

import java.io.Serializable;

public class MatchStatModel implements Serializable {

    private String aces;
    private String winners;
    private String doubleFaults;
    private String errors;
    private String firstServeWon;
    private String totalFirstServe;
    private String secondServeWon;
    private String totalSecondServe;
    private String breakPointsFaced;
    private String breakPointsConverted;

    public MatchStatModel() {}

    public String getAces() {
        return aces;
    }

    public void setAces(String aces) {
        this.aces = aces;
    }

    public String getWinners() {
        return winners;
    }

    public void setWinners(String winners) {
        this.winners = winners;
    }

    public String getDoubleFaults() {
        return doubleFaults;
    }

    public void setDoubleFaults(String doubleFaults) {
        this.doubleFaults = doubleFaults;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }

    public String getFirstServeWon() {
        return firstServeWon;
    }

    public void setFirstServeWon(String firstServeWon) {
        this.firstServeWon = firstServeWon;
    }

    public String getSecondServeWon() {
        return secondServeWon;
    }

    public void setSecondServeWon(String secondServeWon) {
        this.secondServeWon = secondServeWon;
    }

    public String getBreakPointsFaced() {
        return breakPointsFaced;
    }

    public void setBreakPointsFaced(String breakPointsFaced) {
        this.breakPointsFaced = breakPointsFaced;
    }

    public String getBreakPointsConverted() {
        return breakPointsConverted;
    }

    public void setBreakPointsConverted(String breakPointsConverted) {
        this.breakPointsConverted = breakPointsConverted;
    }

    public String getTotalFirstServe() {
        return totalFirstServe;
    }

    public void setTotalFirstServe(String totalFirstServe) {
        this.totalFirstServe = totalFirstServe;
    }

    public String getTotalSecondServe() {
        return totalSecondServe;
    }

    public void setTotalSecondServe(String totalSecondServe) {
        this.totalSecondServe = totalSecondServe;
    }
}
