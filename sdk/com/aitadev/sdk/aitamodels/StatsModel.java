package com.aitadev.sdk.aitamodels;

import java.io.Serializable;

public class StatsModel implements Serializable {
    private String tid;
    private String mid;
    private String tname;
    private String status;
    private String winner;
    private String winStrategy;
    private String servingPlayer;
    private PlayerScoreStatsModel player1ScoreStatsModel;
    private PlayerScoreStatsModel player2ScoreStatsModel;

    public StatsModel(){}

    public String getWinStrategy() {
        return winStrategy;
    }

    public void setWinStrategy(String winStrategy) {
        this.winStrategy = winStrategy;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public PlayerScoreStatsModel getPlayer1ScoreStatsModel() {
        return player1ScoreStatsModel;
    }

    public void setPlayer1ScoreStatsModel(PlayerScoreStatsModel player1ScoreStatsModel) {
        this.player1ScoreStatsModel = player1ScoreStatsModel;
    }

    public PlayerScoreStatsModel getPlayer2ScoreStatsModel() {
        return player2ScoreStatsModel;
    }

    public void setPlayer2ScoreStatsModel(PlayerScoreStatsModel player2ScoreStatsModel) {
        this.player2ScoreStatsModel = player2ScoreStatsModel;
    }

    public String getServingPlayer() {
        return servingPlayer;
    }

    public void setServingPlayer(String servingPlayer) {
        this.servingPlayer = servingPlayer;
    }
}
