package com.aitadev.sdk.aitamodels;

import java.io.Serializable;
import java.util.List;

public class JsonPlayerScoreStatsModel implements Serializable {

    private String name;
    private String aitaId;
    private String matchScore;
    private List<SetModel> sets;
    private MatchStatModel stats;

    public JsonPlayerScoreStatsModel(){}

    public String getMatchScore() {
        return matchScore;
    }

    public void setMatchScore(String matchScore) {
        this.matchScore = matchScore;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAitaId() {
        return aitaId;
    }

    public void setAitaId(String aitaId) {
        this.aitaId = aitaId;
    }

    public List<SetModel> getSets() {
        return sets;
    }

    public void setSets(List<SetModel> sets) {
        this.sets = sets;
    }

    public MatchStatModel getStats() {
        return stats;
    }

    public void setStats(MatchStatModel stats) {
        this.stats = stats;
    }
}
