package com.aitadev.sdk.aitamodels;

public class AdModelUI extends AdModel {
    private String aid;

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }
}
