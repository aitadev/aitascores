package com.aitadev.sdk.aitamodels;

import java.io.Serializable;

public class PlayerScoreStatsModel  implements Serializable {

    private String name;
    private String aitaId;
    private String matchScore;
    private ListModel<SetModel> setList;
    private MatchStatModel matchStatModel;

    public PlayerScoreStatsModel(){}

    public String getMatchScore() {
        return matchScore;
    }

    public void setMatchScore(String matchScore) {
        this.matchScore = matchScore;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAitaId() {
        return aitaId;
    }

    public void setAitaId(String aitaId) {
        this.aitaId = aitaId;
    }

    public ListModel<SetModel> getSetList() {
        return setList;
    }

    public void setSetList(ListModel<SetModel> setList) {
        this.setList = setList;
    }

    public MatchStatModel getMatchStatModel() {
        return matchStatModel;
    }

    public void setMatchStatModel(MatchStatModel matchStatModel) {
        this.matchStatModel = matchStatModel;
    }
}
