package com.aitadev.sdk.aitamodels;

import java.io.Serializable;

public class SetModel implements Serializable {

    private String gameScore;
    private String pointScore;
    private String Result;
    //private MatchStatModel setStatModel;

    public SetModel(){}

    public String getGameScore() {
        return gameScore;
    }

    public void setGameScore(String gameScore) {
        this.gameScore = gameScore;
    }

    public String getPointScore() {
        return pointScore;
    }

    public void setPointScore(String pointScore) {
        this.pointScore = pointScore;
    }

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }
//
//    public MatchStatModel getSetStatModel() {
//        return setStatModel;
//    }
//
//    public void setSetStatModel(MatchStatModel setStatModel) {
//        this.setStatModel = setStatModel;
//    }
}
