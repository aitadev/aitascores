package com.aitadev.sdk.aitamodels;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class TournamentModel {
    private String tid;
    private String aid;
    private String tname;
    private String startDate;
    private String tcategory;

    public TournamentModel(String tid, String tname, String startDate, String tcategory) {
        this.tid = tid;
        this.tname = tname;
        this.startDate = startDate;
        this.tcategory = tcategory;
    }

    public TournamentModel() {}

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getTcategory() {
        return tcategory;
    }

    public void setTcategory(String tcategory) {
        this.tcategory = tcategory;
    }

}
