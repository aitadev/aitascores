package com.aitadev.sdk.aitamodels;

public class MatchModel {
    private String mid;
    private String servingPlayer;
    private String winner;
    private String status;
    private String winStrategy;
    private PlayerScoreModel player1ScoreModel;
    private PlayerScoreModel player2ScoreModel;

    public MatchModel(){}

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public String getWinStrategy() {
        return winStrategy;
    }

    public void setWinStrategy(String winStrategy) {
        this.winStrategy = winStrategy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public PlayerScoreModel getPlayer1ScoreModel() {
        return player1ScoreModel;
    }

    public void setPlayer1ScoreModel(PlayerScoreModel player1ScoreModel) {
        this.player1ScoreModel = player1ScoreModel;
    }

    public PlayerScoreModel getPlayer2ScoreModel() {
        return player2ScoreModel;
    }

    public void setPlayer2ScoreModel(PlayerScoreModel player2ScoreModel) {
        this.player2ScoreModel = player2ScoreModel;
    }

    public String getServingPlayer() {
        return servingPlayer;
    }

    public void setServingPlayer(String servingPlayer) {
        this.servingPlayer = servingPlayer;
    }
}
