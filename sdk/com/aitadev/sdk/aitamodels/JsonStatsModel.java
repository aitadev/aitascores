package com.aitadev.sdk.aitamodels;

public class JsonStatsModel {
    private String tid;
    private String mid;
    private String tname;
    private String status;
    private String result;
    private String winStrategy;
    private String servingPlayer;
    private JsonPlayerScoreStatsModel player1;
    private JsonPlayerScoreStatsModel player2;

    public JsonStatsModel(){

    }

    public String getWinStrategy() {
        return winStrategy;
    }

    public void setWinStrategy(String winStrategy) {
        this.winStrategy = winStrategy;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getServingPlayer() {
        return servingPlayer;
    }

    public void setServingPlayer(String servingPlayer) {
        this.servingPlayer = servingPlayer;
    }

    public JsonPlayerScoreStatsModel getPlayer1() {
        return player1;
    }

    public void setPlayer1(JsonPlayerScoreStatsModel player1) {
        this.player1 = player1;
    }

    public JsonPlayerScoreStatsModel getPlayer2() {
        return player2;
    }

    public void setPlayer2(JsonPlayerScoreStatsModel player2) {
        this.player2 = player2;
    }
}
