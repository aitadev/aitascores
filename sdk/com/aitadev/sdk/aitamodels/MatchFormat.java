package com.aitadev.sdk.aitamodels;

import java.io.Serializable;

public class MatchFormat implements Serializable {

    private String bestOfNSetsPerMatch;
    private String numPointsPerTieBreak;
    private String bestOfNgamesPerMatch;
    private String singlePointDeuce;
    private String isMatchSetFormat;

    public MatchFormat(String bestOfNSetsPerMatch, String bestOfNGamesPerSet, String numPointsPerTieBreak, String singlePointDeuce) {
        this.bestOfNSetsPerMatch = bestOfNSetsPerMatch;
        this.numPointsPerTieBreak = numPointsPerTieBreak;
        this.singlePointDeuce = singlePointDeuce;
    }

    public MatchFormat(String bestOfNSetsPerMatch, String singlePointDeuce) {
        this.bestOfNSetsPerMatch = bestOfNSetsPerMatch;
        this.singlePointDeuce = singlePointDeuce;
    }
    public MatchFormat(){

    }

    public String getIsMatchSetFormat() {
        return isMatchSetFormat;
    }

    public void setIsMatchSetFormat(String matchSetFormat) {
        isMatchSetFormat = matchSetFormat;
    }

    public String getBestOfNgamesPerMatch() {
        return bestOfNgamesPerMatch;
    }

    public void setBestOfNgamesPerMatch(String bestOfNgamesPerMatch) {
        this.bestOfNgamesPerMatch = bestOfNgamesPerMatch;
    }

    public String getNumPointsPerTieBreak() {
        return numPointsPerTieBreak;
    }

    public void setNumPointsPerTieBreak(String numPointsPerTieBreak) {
        this.numPointsPerTieBreak = numPointsPerTieBreak;
    }

    public String getBestOfNSetsPerMatch() {
        return bestOfNSetsPerMatch;
    }

    public void setBestOfNSetsPerMatch(String bestOfNSetsPerMatch) {
        this.bestOfNSetsPerMatch = bestOfNSetsPerMatch;
    }


    public String isSinglePointDeuce() {
        return singlePointDeuce;
    }

    public void setSinglePointDeuce(String singlePointDeuce) {
        this.singlePointDeuce = singlePointDeuce;
    }

}
