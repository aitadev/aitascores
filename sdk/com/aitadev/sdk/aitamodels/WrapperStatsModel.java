package com.aitadev.sdk.aitamodels;

public class WrapperStatsModel {

    private StatsModel statsModel;

    public WrapperStatsModel(){

    }

    public StatsModel getStatsModel() {
        return statsModel;
    }

    public void setStatsModel(StatsModel statsModel) {
        this.statsModel = statsModel;
    }
}
