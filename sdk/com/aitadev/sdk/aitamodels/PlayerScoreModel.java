package com.aitadev.sdk.aitamodels;

public class PlayerScoreModel {

    private String aitaId;
    private String name;
    private String pointScore;
    private String gameScore;
    private String matchScore;

    public PlayerScoreModel(){}

    public String getAitaId() {
        return aitaId;
    }

    public void setAitaId(String aitaId) {
        this.aitaId = aitaId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPointScore() {
        return pointScore;
    }

    public void setPointScore(String pointScore) {
        this.pointScore = pointScore;
    }

    public String getGameScore() {
        return gameScore;
    }

    public void setGameScore(String gameScore) {
        this.gameScore = gameScore;
    }

    public String getMatchScore() {
        return matchScore;
    }

    public void setMatchScore(String matchScore) {
        this.matchScore = matchScore;
    }

}
