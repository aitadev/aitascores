package com.aitadev.sdk.aitamodels;

import java.util.List;

public class DrawModel {
    private String roundNumber;
    private String roundName;
    private String tid;
    private List<MatchModel> matchModels;

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getRoundNumber() {
        return roundNumber;
    }

    public void setRoundNumber(String roundNumber) {
        this.roundNumber = roundNumber;
    }

    public String getRoundName() {
        return roundName;
    }

    public void setRoundName(String roundName) {
        this.roundName = roundName;
    }

    public List<MatchModel> getMatchModels() {
        return matchModels;
    }

    public void setMatchModels(List<MatchModel> matchModels) {
        this.matchModels = matchModels;
    }
}
