package com.aitadev.sdk.aitamodels;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AdModelJsonFormat implements Serializable {
    private String aid;
    private List<AdModel> adList;

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public List<AdModel> getAdList() {
        return adList;
    }

    public void setAdList(List<AdModel> adList) {
        this.adList = adList;
    }
}
