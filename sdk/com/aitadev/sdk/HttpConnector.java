package com.aitadev.sdk;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpConnector {
    private static final int DEFAULT_SERVER_TIMEOUT_MS = 13000;

    public static HttpURLConnection connect(ConnectionTask task) throws IOException {

        URL url = new URL(task.url);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        //CONECTION PROPS
        conn.setRequestMethod(task.requestMethod.name());
        conn.setConnectTimeout(DEFAULT_SERVER_TIMEOUT_MS);
        conn.setReadTimeout(DEFAULT_SERVER_TIMEOUT_MS);
        conn.setInstanceFollowRedirects(true);
        conn.setDoInput(true);

        if ((task.requestMethod == ConnectionTask.RequestMethod.POST) || (task.requestMethod == ConnectionTask.RequestMethod.PUT)||(task.requestMethod == ConnectionTask.RequestMethod.PATCH)) {
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-length", (task.body != null) ? ("" + task.body.length) : "0");
        }

        return conn;
    }
}
