package com.aitadev.sdk;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.aitadev.sdk.ConnectionTask.RequestMethod;

import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;

public class RESTExecutor extends AsyncTask<String, String, String> {

    private ConnectionTask task;
    private Context context;

    private ProgressDialog progressDialog;

    public RESTExecutor(ConnectionTask connectionTask) {
        this.context = connectionTask.context;
        this.task = connectionTask;
    }

    @Override
    protected void onPreExecute() {

        //Executed on ui thread

        super.onPreExecute();

        progressDialog =new ProgressDialog(context);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Fetching data...Please wait");
        progressDialog.show();

    }


    @Override
    protected String doInBackground(String... strings) {

        HttpURLConnection urlConnection = null;

        InputStream is = null;
        BufferedReader br = null;
        OutputStream os = null;

        try {
            urlConnection = HttpConnector.connect(task);

            if ((task.requestMethod == RequestMethod.POST) || (task.requestMethod == RequestMethod.PUT) || (task.requestMethod == RequestMethod.PATCH)) {
                is = new ByteArrayInputStream((task.body != null) ? task.body : "".getBytes());
                os = new DataOutputStream(urlConnection.getOutputStream());
                os.write(task.body);

                if (is != null) {
                    try {
                        is.close();
                    } catch (final Exception e) {
                        e.printStackTrace();
                    }
                    is = null;
                }

                if (os != null) {
                    try {
                        os.close();
                    } catch (final Exception e) {
                        e.printStackTrace();
                    }
                    os = null;
                }
            }

            /* Response for GET or POST */
            if (urlConnection.getResponseCode() == urlConnection.HTTP_OK) {

                //GET INPUT FROM STREAM
                is = new BufferedInputStream(urlConnection.getInputStream());
                br = new BufferedReader(new InputStreamReader(is));

                String line;
                StringBuffer jsonData = new StringBuffer();

                //READ
                while ((line = br.readLine()) != null) {
                    jsonData.append(line + "\n");
                }

                //CLOSE RESOURCES
                try {
                    br.close();
                } catch (final Exception e) {
                    e.printStackTrace();
                }
                try {
                    is.close();
                } catch (final Exception e) {
                    e.printStackTrace();
                }

                //RETURN JSON
                return jsonData.toString();

            } else {
                return "Error " + urlConnection.getResponseMessage();
            }
        } catch (final IOException e) {

            e.printStackTrace();
            return "Error "+ e.getMessage();

        } finally {
            /* clear unused resources */
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                is = null;
            }
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                br = null;
            }
            if(urlConnection != null) {
                urlConnection.disconnect();
            }
        }


    }

    @Override
    protected void onPostExecute(String jsonData) {
        //Executed on ui thread

        super.onPostExecute(jsonData);

        progressDialog.dismiss();
        if(jsonData.startsWith("Error"))
        {
            String error=jsonData;
            try {
                // TODO: Handle null data in view to avoid app crash

                task.listener.onConnectionComplete(task,null);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
        }else {

            //PARSER
            try {
                task.listener.onConnectionComplete(task,jsonData);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }

    }

}
