package com.aitadev.sdk;

import com.aitadev.sdk.api.IApiNodeParser;
import com.aitadev.sdk.api.IConnectionTaskListener;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;

import org.json.JSONException;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;


public class ConnectionManager {

    private static ConnectionManager mSharedInstance = null;

    private ConnectionManager(){}

    public static ConnectionManager getSharedInstance() {
        if (mSharedInstance == null)
            return new ConnectionManager();
        return mSharedInstance;
    }

    public static abstract class ConnectionTaskListenerAdapter implements IConnectionTaskListener {
        @Override
        public boolean onConnectionStart(final ConnectionTask task) {
            return true;
        }

        @Override
        public void onConnectionResponse(final ConnectionTask task, final Map<String, String> headers, final int status) {
        }

        @Override
        public void onConnectionComplete(final ConnectionTask task, final String json) throws JSONException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException, IOException {
            parse(task,json);

        }

        private void parse(ConnectionTask task, String json) throws IOException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {

            if(json == null) {
                throw new IOException("No data from server");
            }

            JsonParser jsonParser = new JsonFactory().createParser(json);
            IApiNodeParser parser = task.apiNode.getNodeParser();
            task.response = parser.parse(jsonParser, task.apiNode.parseResponseJSON());
        }


        @Override
        public void onConnectionFailed(final ConnectionTask task, final IOException exception) {
            if (exception != null) {
                exception.printStackTrace();
            }
        }

        @Override
        public void onConnectionCanceled(final ConnectionTask task) {
        }
    }

    public void doGet(final ConnectionTask task, final IConnectionTaskListener listener) {

        if (task == null || listener == null) {
            return;
        }

        if (listener != null) {
            task.listener = listener;
        }
        new RESTExecutor(task).execute();
    }

    public void doPost(final ConnectionTask task, final IConnectionTaskListener listener) {

        if (task == null || listener == null) {
            return;
        }

        if (listener != null) {
            task.listener = listener;
        }
        new RESTExecutor(task).execute();
    }
}
