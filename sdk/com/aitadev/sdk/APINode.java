package com.aitadev.sdk;

import com.aitadev.sdk.aitamodels.AdModel;
import com.aitadev.sdk.aitamodels.AdModelUI;
import com.aitadev.sdk.aitamodels.MatchFormat;
import com.aitadev.sdk.aitamodels.MatchModel;
import com.aitadev.sdk.aitamodels.StatsModel;
import com.aitadev.sdk.aitamodels.TournamentModel;
import com.aitadev.sdk.api.IApiNodeParser;
import com.aitadev.sdk.jsonParsers.AdDetailsApiNodeParser;
import com.aitadev.sdk.jsonParsers.MatchFormatApiNodeParser;
import com.aitadev.sdk.jsonParsers.MatchListApiNodeParser;
import com.aitadev.sdk.jsonParsers.StatsListApiNodeParser;
import com.aitadev.sdk.jsonParsers.TournamentListApiNodeParser;

public enum APINode {
    UNKNOWN,
    TOURNAMNENT_CREATE {
        @Override
        public String getURL(){
            return API_ENDPOINT + "/tlist/create/";
        }


    },
    TOURNAMENT_LIST {
        @Override
        public String getURL(){
            return API_ENDPOINT + "/aitaplatform/tournaments/tournamentlist";
        }

        @Override
        public String[] parseResponseJSON() {
            return new String[] {"tid","aid","tname","startDate","tcategory"};
        }

        @Override
        public IApiNodeParser getNodeParser() {
            return new TournamentListApiNodeParser();
        }

        @Override
        public Class getModelClass() {
            return TournamentModel.class;
        }
    },

    MATCH_LIST {
        @Override
        public String getURL(){
            return API_ENDPOINT + "/aitaplatform/tournamentschedule/{tid}/matchlist/{status}";
        }

        @Override
        public String[] parseResponseJSON() {
            return new String[] {"tid","mid","servingPlayer","winner","status","aitaId","name","pointScore","gameScore","matchScore","winStrategy"};
        }

        @Override
        public IApiNodeParser getNodeParser() { return new MatchListApiNodeParser(); }

        @Override
        public Class getModelClass() {
            return MatchModel.class;
        }
    },
    MATCH_FORMAT {
        @Override
        public String getURL(){
            return API_ENDPOINT + "/aitaplatform/tournamentschedule/{tid}/{mid}/matchFormat";
        }

        @Override
        public String[] parseResponseJSON() {
            return new String[] {"bestOfNSetsPerMatch","numPointsPerTieBreak","bestOfNgamesPerMatch","singlePointDeuce","isMatchSetFormat"};
        }

        @Override
        public IApiNodeParser getNodeParser() { return new MatchFormatApiNodeParser(); }

        @Override
        public Class getModelClass() {
            return MatchFormat.class;
        }
    },
    LIVE_SCORE_STATS_POST {
        @Override
        public String getURL(){
            return API_ENDPOINT + "/aitaplatform/tournamentstats/update";
        }

        @Override
        public String[] parseResponseJSON() {
            return new String[] {"tid","mid","tname", "status", "aitaId","name","pointScore","gameScore","servingPlayer", "setScore","matchScore","winner","result","aces", "winners","doubleFaults","errors","totalFirstServe","firstServeWon","totalSecondServe","secondServeWon","breakPointsConverted","breakPointsFaced","winStrategy"};
        }

        @Override
        public IApiNodeParser getNodeParser() { return new StatsListApiNodeParser(); }

        @Override
        public Class getModelClass() {
            return StatsModel.class;
        }
    },
    ADMIN_LIVE_SCORE_STATS {
        @Override
        public String getURL(){
            return API_ENDPOINT + "/aitaplatform/tournamentstats/admin/{tid}/{mid}";
        }

        @Override
        public String[] parseResponseJSON() {
            return new String[] {"tid","mid","tname", "status", "aitaId","name","pointScore","gameScore","setScore","servingPlayer", "matchScore","winner","result","aces", "winners","doubleFaults","errors","totalFirstServe","totalSecondServe","firstServeWon","secondServeWon","breakPointsConverted","breakPointsFaced","winStrategy"};
        }

        @Override
        public IApiNodeParser getNodeParser() { return new StatsListApiNodeParser(); }

        @Override
        public Class getModelClass() {
            return StatsModel.class;
        }
    },
    LIVE_SCORE_STATS {
        @Override
        public String getURL(){
            return API_ENDPOINT + "/aitaplatform/tournamentstats/{tid}/{mid}";
        }

        @Override
        public String[] parseResponseJSON() {
            return new String[] {"tid","mid","tname", "status", "aitaId","name","pointScore","gameScore","setScore","servingPlayer", "matchScore","winner","result","aces", "winners","doubleFaults","errors","totalFirstServe","totalSecondServe","firstServeWon","secondServeWon","breakPointsConverted","breakPointsFaced","winStrategy"};
        }

        @Override
        public IApiNodeParser getNodeParser() { return new StatsListApiNodeParser(); }

        @Override
        public Class getModelClass() {
            return StatsModel.class;
        }
    },
    AD_DETAILS {
        @Override
        public String getURL(){
            return API_ENDPOINT + "/aitaplatform/adDetails/{aid}";
        }

        @Override
        public String[] parseResponseJSON() {
            return new String[] {"aid","location","numClayCourts","numHardCourts","numFloodLights","numCoaches","numAitaPlayers","headCoach","headCoachContact","isFitnessAvailable","feeStructure"};
        }

        @Override
        public IApiNodeParser getNodeParser() {
            return new AdDetailsApiNodeParser();
        }

        @Override
        public Class getModelClass() {
            return AdModelUI.class;
        }
    };

    public static final String API_ENDPOINT = "http://ec2-3-6-50-15.ap-south-1.compute.amazonaws.com:8080";

    public String getURL() {
        return "UNKNOWN";
    }

    public String[] parseRequestJSON() {
        return new String[] {};
    }

    public String[] parseResponseJSON() {
        return new String[] {};
    }

    public Class getModelClass() {
        return null;
    }

    public IApiNodeParser getNodeParser(){return null;}
}
