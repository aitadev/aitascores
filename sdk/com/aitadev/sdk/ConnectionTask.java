package com.aitadev.sdk;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.aitadev.sdk.aitamodels.JsonPlayerScoreStatsModel;
import com.aitadev.sdk.aitamodels.JsonStatsModel;
import com.aitadev.sdk.aitamodels.ListModel;
import com.aitadev.sdk.aitamodels.StatsModel;
import com.aitadev.sdk.api.IConnectionTaskListener;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import java.util.Map;

public class ConnectionTask {
    public enum RequestMethod {
        HEAD, GET, POST, PUT, PATCH, DELETE
    }
    public RequestMethod requestMethod = RequestMethod.GET;
    public IConnectionTaskListener listener = null;
    public byte[] body = null;
    public View view;
    public APINode apiNode;
    public String url;
    public ListModel<Object> response;


    public Context context;

    public static ConnectionTask obtainGetTask(final APINode apiNode, final Context c, View view, Map<String ,String> patternValue) {
        final ConnectionTask task = new ConnectionTask();
        task.apiNode = apiNode;
        String url = task.apiNode.getURL();
        for (String key : patternValue.keySet()) {
            if(url.contains(key)) {
                url = url.replace(key, patternValue.get(key));
            }
        }
        task.url = url;
        Log.d("*** endpoint url is: " , task.url);
        task.requestMethod = RequestMethod.GET;
        task.context = c;
        task.view = view;
        task.response = null;
        return task;
    }

    public static ConnectionTask obtainPOSTTask(final APINode apiNode, final Context c, View view, Map<String ,String> patternValue, Object requestObj) throws JsonProcessingException {
        final ConnectionTask task = new ConnectionTask();
        ObjectWriter ow = new ObjectMapper().writer();

        JsonStatsModel jsonStatsModel = new JsonStatsModel();
        JsonPlayerScoreStatsModel player1ScoreStatsModel = new JsonPlayerScoreStatsModel();
        JsonPlayerScoreStatsModel player2ScoreStatsModel = new JsonPlayerScoreStatsModel();

        StatsModel statsModel = (StatsModel) requestObj;

        jsonStatsModel.setTid(statsModel.getTid());
        jsonStatsModel.setMid(statsModel.getMid());
        jsonStatsModel.setStatus(statsModel.getStatus());
        jsonStatsModel.setServingPlayer(statsModel.getServingPlayer());
        jsonStatsModel.setResult(statsModel.getWinner());
        jsonStatsModel.setWinStrategy(statsModel.getWinStrategy());

        player1ScoreStatsModel.setAitaId(statsModel.getPlayer1ScoreStatsModel().getAitaId());
        player1ScoreStatsModel.setName(statsModel.getPlayer1ScoreStatsModel().getName());
        player1ScoreStatsModel.setMatchScore(statsModel.getPlayer1ScoreStatsModel().getMatchScore());
        player1ScoreStatsModel.setStats(statsModel.getPlayer1ScoreStatsModel().getMatchStatModel());
        //player1ScoreStatsModel.setWinner(statsModel.getWinner());
        player1ScoreStatsModel.setSets(statsModel.getPlayer1ScoreStatsModel().getSetList().items);
        jsonStatsModel.setPlayer1(player1ScoreStatsModel);

        player2ScoreStatsModel.setAitaId(statsModel.getPlayer2ScoreStatsModel().getAitaId());
        player2ScoreStatsModel.setName(statsModel.getPlayer2ScoreStatsModel().getName());
        player2ScoreStatsModel.setMatchScore(statsModel.getPlayer2ScoreStatsModel().getMatchScore());
        //player2ScoreStatsModel.setWinner(statsModel.getWinner());
        player2ScoreStatsModel.setStats(statsModel.getPlayer2ScoreStatsModel().getMatchStatModel());
        player2ScoreStatsModel.setSets(statsModel.getPlayer2ScoreStatsModel().getSetList().items);
        jsonStatsModel.setPlayer2(player2ScoreStatsModel);


        task.body = ow.writeValueAsBytes(jsonStatsModel);// ow.writeValueAsBytes(json.getBytes());
        task.apiNode = apiNode;
        String url = task.apiNode.getURL();
        for (String key : patternValue.keySet()) {
            if(url.contains(key)) {
                url = url.replace(key, patternValue.get(key));
            }
        }
        task.url = url;
        Log.d("***sheera*** body is: " , task.body.toString());
        task.requestMethod = RequestMethod.POST;
        task.context = c;
        task.view = view;
        task.response = null;
        return task;
    }
}
