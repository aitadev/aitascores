AitaScores app is tied to aitaplatform services. It utilises the aitaplatform
endpoints to get the player data and their stats with match scheduling.


Since aitaplatform is under development, to use this app, we need to manually
specify the ip of aitaplatform service in APINode.java. Once specified, generate
an apk and flash it.